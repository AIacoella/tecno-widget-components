import React, { FunctionComponent, useState } from 'react';
import * as Widgets from 'tecno-widget-components';
import * as PDFWidgets from 'tecno-widget-components/dist/PDFWidgets';
import {
  PDFViewer,
  Page,
  Document,
  View,
  StyleSheet,
} from '@react-pdf/renderer';
import 'tecno-widget-components/dist/index.css';
import './assets/css/style.css';

const App = () => {
  const TestedComponents = [
    'PieChart',
    'Clock',
    'Image',
    'BarChart',
    'Gauge',
    'TimeSeriesTecno',
    'ParagraphTecno',
    'TableTecno',
    'HeaderTecno',
  ];

  return (
    <div style={{ marginBottom: 300 }}>
      <h1>Widget Playground</h1>
      {TestedComponents.map(key => {
        const props = Widgets.mockProps[key];
        if (!props) {
          console.log(TestedComponents);
          console.log(key);
          console.log(Widgets.mockProps);
          console.warn(
            "Can't test component " + key + '. No testing props provided'
          );
          return null;
        }
        const Widget = Widgets[key];
        if (!Widget) {
          console.warn("Can't test component " + key + '. NULL');
          return null;
        }
        const PDFWidgetRenderer = PDFWidgets[key + 'PDF'];
        return (
          <WidgetShowcase
            key={key}
            name={key}
            PDFWidgetRenderer={PDFWidgetRenderer}
            Widget={Widget}
            props={props}
          />
        );
      })}
    </div>
  );
};

export default App;

const WidgetShowcase: FunctionComponent<{
  name: string;
  PDFWidgetRenderer?: any;
  Widget: any;
  props: any;
}> = ({ name, PDFWidgetRenderer, Widget, props }) => {
  const [zoom, setZoom] = useState(1);

  return (
    <div className="showcase-container">
      <div className="showcase-header">
        <h1>{name}</h1>
        <div className="zoom-container">
          <input
            type="radio"
            id={`${name}-small`}
            checked={zoom === 0.75}
            onClick={() => setZoom(0.75)}
          />
          <label htmlFor={`${name}-small`}>Small (0.75)</label>
          <input
            type="radio"
            id={`${name}-medium`}
            checked={zoom === 1}
            onClick={() => setZoom(1)}
          />
          <label htmlFor={`${name}-medium`}>Medium (1.00)</label>
          <input
            type="radio"
            id={`${name}-big`}
            checked={zoom === 1.25}
            onClick={() => setZoom(1.25)}
          />
          <label htmlFor={`${name}-big`}>Big (1.25)</label>
        </div>
      </div>
      <div className="widget-row">
        <div className={'showcase-small '}>
          <Widget {...props} scale={zoom} />
        </div>
        <div className={'showcase-big '}>
          <Widgets.TecnoSnapshotContainer title="Test" scale={zoom}>
            <Widget {...props} scale={zoom} />
          </Widgets.TecnoSnapshotContainer>
        </div>
      </div>
      {PDFWidgetRenderer && <h3>PDF View</h3>}
      {PDFWidgetRenderer && (
        <PDFViewer style={{ width: 500, height: 500 }}>
          <Document>
            <Page size="A4" style={{ padding: 10 }}>
              <View style={pdfStyles.row} wrap={true}>
                <View style={pdfStyles.view1} wrap={true}>
                  {PDFWidgetRenderer({ ...props, scale: zoom })}
                </View>
                <View style={pdfStyles.view2} wrap={true}>
                  {PDFWidgetRenderer({ ...props, scale: zoom })}
                </View>
              </View>
              <View style={pdfStyles.row} wrap={true}>
                <View style={pdfStyles.view3} wrap={true}>
                  {PDFWidgetRenderer({ ...props, scale: zoom })}
                </View>
              </View>
            </Page>
          </Document>
        </PDFViewer>
      )}
    </div>
  );
};

const pdfStyles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
  },
  view1: {
    flex: 0.4,
    flexDirection: 'column',
    padding: 10,
  },
  view2: {
    flex: 0.6,
    flexDirection: 'column',
    padding: 10,
  },
  view3: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
  },
});
