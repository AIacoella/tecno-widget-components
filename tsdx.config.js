const postcss = require('rollup-plugin-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const path = require('path');
const image = require('@rollup/plugin-image');

module.exports = {
  rollup(config, options) {
    config.input = ['./src/index.ts', './src/PDFWidgets.ts'];
    (config.output.file = undefined), (config.output.dir = 'dist');
    config.plugins.push(
      postcss({
        plugins: [
          autoprefixer(),
          cssnano({
            preset: 'default',
          }),
        ],

        inject: false,
        // only write out CSS for the first bundle (avoids pointless extra files):
        extract: path.resolve('dist/index.css'),
      })
    );
    config.plugins.unshift(image());
    return config;
  },
};
