import React, {
  useState,
  cloneElement,
  isValidElement,
  useEffect,
} from 'react';
import styles from './styles.module.css';
import WidgetHeader from './components/WidgetHeader/WidgetHeader';
import {
  buildBorderStyle,
  buildStyleString,
  GlobalParametersInterface,
  hex,
  OptionType,
} from './utils';
import WidgetWarning from './components/WidgetWarning/WidgetWarning';

interface WidgetContainerProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  rss?: boolean;
  asyncRender?: boolean;
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;

  contentContainerClassName?: string;
  contentContainerStyle?: React.CSSProperties;
  //Style props
  scale: number;
  margin?: FourIntegerType;
  padding?: FourIntegerType;
  border?: FourBorderType;
  backgroundColor?: string;
  containerRef?: React.MutableRefObject<any>;
  titleColor?: string;
  titleBackgroundColor?: string;
  warning?: string;
}

export interface WidgetProps {
  rss?: boolean; //Render-server-side : used to distinguish between a use of the widget-component in the report-engine and in the WebApp
  //Style props common to every widget
  scale: number;
  margin?: FourIntegerType;
  padding?: FourIntegerType;
  border?: FourBorderType;
  backgroundColor?: string;
  //Generic props common to every widget
  globalParameters?: GlobalParametersInterface;
}

type V = string | number;
export type FourIntegerType = [V, V, V, V];
type BT = { borderWidth?: string | number; borderColor?: string };
export type FourBorderType = [BT, BT, BT, BT];

export const WidgetContainer: React.FunctionComponent<WidgetContainerProps> = ({
  rss,
  asyncRender,
  children,
  title,
  titleScale = 1,
  className,
  contentContainerClassName,
  contentContainerStyle,
  scale,
  margin = [0, 0, 0, 0],
  padding = [0, 0, 0, 0],
  border = [{}, {}, {}, {}],
  backgroundColor,
  style,
  containerRef,
  titleColor,
  titleBackgroundColor,
  warning,
  titleAlign,
  ...rest
}) => {
  const [rendered, setRendered] = useState(false);

  const hasRendered = () => {
    setRendered(true);
  };

  useEffect(() => {
    if (!asyncRender) hasRendered();
    else
      setTimeout(() => {
        if (!rendered) hasRendered();
      }, 5000);
  }, []);

  const childrenWithProps = React.Children.map(children, child => {
    if (isValidElement(child))
      return cloneElement(child, { hasRendered: hasRendered });
    return child;
  });

  const widthMargin = Number(margin[1]) + Number(margin[3]);
  const heightMargin = Number(margin[0]) + Number(margin[2]);

  const marginStr = buildStyleString(margin, 'px');
  const paddingStr = buildStyleString(padding, 'px');
  const borderStyle = buildBorderStyle(border);

  return (
    <div
      className={styles.widgetContainerContainer}
      style={{
        backgroundColor: hex(backgroundColor),
      }}
    >
      {warning && <WidgetWarning warning={warning} />}
      <div
        className={styles.widgetContainer + ' ' + (className ? className : '')}
        style={{
          ...style,
          width: `calc(100% - ${widthMargin}px)`,
          height: `calc(100% - ${heightMargin}px)`,
          margin: marginStr,
          padding: paddingStr,
          borderStyle: 'solid',
          borderWidth: borderStyle.width,
          borderColor: borderStyle.color,
        }}
        {...rest}
      >
        {rss && rendered && <span id="rendered" />}
        {title && (
          <WidgetHeader
            title={title}
            scale={titleScale}
            titleColor={titleColor}
            titleBackgroundColor={titleBackgroundColor}
            titleAlign={titleAlign}
          />
        )}
        <div
          className={
            styles.widgetContentContainer +
            ' ' +
            (contentContainerClassName ? contentContainerClassName : '')
          }
          style={{ ...contentContainerStyle }}
          ref={containerRef}
        >
          {typeof children === 'function'
            ? children({ hasRendered })
            : childrenWithProps}
        </div>
      </div>
    </div>
  );
};
