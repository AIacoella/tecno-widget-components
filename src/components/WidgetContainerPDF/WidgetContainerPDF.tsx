import React from 'react';
import ReactPDF, { View, Text, StyleSheet } from '@react-pdf/renderer';
import {
  advancedBorder,
  border,
  buildStyleString,
  DEFAULT_LEFT_ALIGN,
  getTextAlign,
  hex,
  OptionType,
} from '../../utils';
import { WIDGET_TITLE_SIZE } from '../../components/WidgetHeader/WidgetHeader';

type V = string | number;
type FourIntegerType = [V, V, V, V];
type BT = { borderWidth?: string | number; borderColor?: string };
type FourBorderType = [BT, BT, BT, BT];

interface WidgetContainerPDFProps extends ReactPDF.ViewProps {
  title?: string;
  titleScale?: number;
  scale: number;
  children?: React.ReactElement;
  style?: any;
  margin?: FourIntegerType;
  padding?: FourIntegerType;
  border?: FourBorderType;
  backgroundColor?: string;
  titleColor?: string;
  titleBackgroundColor?: string;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

export default function WidgetContainerPDF({
  title,
  titleScale = 1,
  scale = 1,
  children,
  style = {},
  margin = [0, 0, 0, 0],
  padding = [0, 0, 0, 0],
  border: borderData = [{}, {}, {}, {}],
  backgroundColor,
  titleBackgroundColor,
  titleColor = '#212224',
  titleAlign = DEFAULT_LEFT_ALIGN,
  ...rest
}: WidgetContainerPDFProps) {
  let additionalStyle = {
    padding: buildStyleString(padding),
    margin: buildStyleString(margin),
    ...advancedBorder(borderData),
    backgroundColor: hex(backgroundColor),
  };

  console.log(additionalStyle);

  return (
    <View style={[styles.container, style, additionalStyle]} {...rest}>
      {title && (
        <Text
          style={[
            styles.header,
            {
              fontSize: titleScale * WIDGET_TITLE_SIZE,
              color: titleColor,
              backgroundColor: titleBackgroundColor,
              textAlign: getTextAlign(titleAlign),
            },
          ]}
        >
          {title}
        </Text>
      )}
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 'auto',
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    color: '#212224',
    paddingHorizontal: 10,
    paddingVertical: 6,
    width: '100%',
    ...border('#eaeaea', [0, 0, 1]),
  },
});
