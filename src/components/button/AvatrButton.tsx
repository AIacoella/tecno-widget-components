import React from 'react'
import styles from './Button.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconName } from '@fortawesome/fontawesome-svg-core'

interface AvatrButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  icon?: IconName | IconRendererType
  solid?: boolean
}

const AvatrButton: React.FunctionComponent<AvatrButtonProps> = ({
  className,
  children,
  icon,
  solid = true,
  ...rest
}) => {
  const renderIcon = (icon: string | IconRendererType) => {
    console.log(typeof icon)
    if (typeof icon == 'string') {
      return (
        <FontAwesomeIcon
          icon={icon as IconName}
          size='sm'
          style={{ marginRight: 8 }}
        />
      )
    } else {
      return icon()
    }
  }

  return (
    <button
      className={`${styles.avatrButton} ${
        !solid ? styles.light : ''
      } ${className}`}
      {...rest}
    >
      {icon && renderIcon(icon)}
      {children}
    </button>
  )
}

export default AvatrButton

type IconRendererType = () => React.ReactElement
