import React from 'react';
import styles from './WidgetWarning.module.css';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import classnames from 'classnames';

export const WIDGET_TITLE_SIZE = 24;

interface WidgetWarningProps {
  warning: string;
}

const WidgetWarning: React.FunctionComponent<WidgetWarningProps> = ({
  warning,
}) => {
  return (
    <div className={styles.container}>
      <OverlayTrigger
        trigger={['hover', 'focus']}
        placement="auto"
        overlay={
          <Popover id="warning-popover" className={styles.tooltipContainer}>
            <Popover.Content>
              <p className={styles.tooltipContent}>{warning}</p>
            </Popover.Content>
          </Popover>
        }
      >
        <div className={styles.warningIcon}>
          <i className={classnames('fas fa-exclamation-triangle')} />
        </div>
      </OverlayTrigger>
    </div>
  );
};

export default WidgetWarning;
