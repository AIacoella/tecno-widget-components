import React from 'react';
import styles from './HtmlMarkdown.module.css';
import marked from 'marked';
import _ from 'lodash';
import { getTextAlign, OptionType } from '../../utils';

type AlignTextType = OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;

interface HtmlMarkdownProps {
  source: string;
  className?: string;
  scale?: number;
  textAlign: AlignTextType;
}

const HtmlMarkdown: React.FunctionComponent<HtmlMarkdownProps> = ({
  source = '',
  className,
  scale = 1,
  textAlign,
}) => {
  const tokens = marked.lexer(source) as Token[];
  console.log(tokens);
  return (
    <div className={styles.container + (className ? ' ' + className : '')}>
      {tokens.map((token, i) => (
        <MarkdownBlock
          scale={scale}
          textAlign={textAlign}
          token={token}
          key={i}
        />
      ))}
    </div>
  );
};

export default HtmlMarkdown;

const MarkdownBlock = ({
  token,
  scale,
  textAlign,
}: {
  token: Token;
  scale: number;
  textAlign: AlignTextType;
}) => {
  const gStyle = { textAlign: getTextAlign(textAlign) };

  switch (token.type) {
    case 'paragraph':
      return (
        <p
          className={styles.p}
          style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
        >
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </p>
      );

    case 'heading':
      const hIndex = Math.min(3, Math.max(1, token.depth || 1));
      const HeadingTag = 'h' + hIndex;
      const headerFontSize = HEADING_SIZE[hIndex - 1];
      return (
        //@ts-ignore
        <HeadingTag style={{ ...gStyle, fontSize: headerFontSize * scale }}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </HeadingTag>
      );

    case 'list':
      if (!token.items) throw new Error("Can't iterate through null items");
      const renderedList = token.items.map((item, i) => (
        <li key={i} style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}>
          <MarkdownLine tokens={item.tokens} scale={scale} />
        </li>
      ));
      if (!token.ordered)
        return (
          <ul
            className={styles.list}
            style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
          >
            {renderedList}
          </ul>
        );
      else
        return (
          <ol
            className={styles.list}
            start={token.start || 1}
            style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
          >
            {renderedList}
          </ol>
        );

    case 'blockquote':
      const blockquoteToken = token.tokens && token.tokens[0];
      return (
        <blockquote
          className={styles.blockquote}
          style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
        >
          {blockquoteToken && (
            <MarkdownBlock
              token={blockquoteToken}
              scale={scale}
              textAlign={textAlign}
            />
          )}
        </blockquote>
      );

    case 'hr':
      return <hr />;

    case 'code':
      return (
        <code
          className={styles.code}
          style={{ ...gStyle, fontSize: CODE_SIZE * scale }}
        >
          <SpaceVivifier text={token.raw} />
        </code>
      );

    case 'space':
      return <br />;

    default:
      return (
        <p
          className={styles.p}
          style={{ ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
        >
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </p>
      );
  }
};

const MarkdownLine = ({ tokens, scale }: { tokens?: Token[]; scale: number }) =>
  !tokens ? null : (
    <React.Fragment>
      {tokens.map((token, i) => (
        <MarkdownLineToken scale={scale} token={token} key={i} />
      ))}
    </React.Fragment>
  );

const MarkdownLineToken = ({
  token,
  scale,
}: {
  token: Token;
  scale: number;
}) => {
  switch (token.type) {
    case 'text':
      return <SpaceVivifier text={token.raw} />;
    case 'strong':
      return (
        <strong className={styles.strong}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </strong>
      );
    case 'em':
      return (
        <em className={styles.italic}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </em>
      );

    case 'link':
      return (
        <a href={token.href} className={styles.link}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </a>
      );

    case 'codespan':
      return (
        <code
          className={styles.codespan}
          style={{ fontSize: CODE_SIZE * scale }}
        >
          {token.raw}
        </code>
      );

    default:
      return <span>{token.raw}</span>;
  }
};

interface Token {
  type: string;
  depth?: number;
  items?: { tokens: Token[] }[];
  raw: string;
  text: string;
  href?: string;
  tokens?: Token[];
  ordered?: boolean;
  start?: number;
}

const SpaceVivifier: React.FunctionComponent<{
  text: string;
}> = ({ text }) => {
  const list = text.split('\n');
  if (list.length === 0) return null;
  if (list.length === 1) return <>{list[0]}</>;
  let lastIsSpace = list[list.length - 1] === '';
  if (lastIsSpace) list.pop();
  return (
    <React.Fragment>
      {list.map((line, i) => (
        <>
          {line}
          <span
            className={
              i !== list.length - 1 || lastIsSpace ? styles.space : undefined
            }
          />
        </>
      ))}
    </React.Fragment>
  );
};

const HEADING_SIZE = [48, 44, 26];
const PARAGRAPH_SIZE = 14;
const CODE_SIZE = 12;
