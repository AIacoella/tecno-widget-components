import React from 'react';
import { View, Text, Link, StyleSheet } from '@react-pdf/renderer';
import marked from 'marked';
import _ from 'lodash';
import { getTextAlign, OptionType } from '../../utils';

type AlignTextType = OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;

interface PDFMarkdownProps {
  source: string;
  scale?: number;
  textAlign: AlignTextType;
}

const PDFMarkdown: React.FunctionComponent<PDFMarkdownProps> = ({
  source,
  scale = 1,
  textAlign,
}) => {
  const tokens = marked.lexer(source) as Token[];
  return (
    <View style={styles.container}>
      {tokens.map((token, i) => (
        <MarkdownBlock
          token={token}
          scale={scale}
          textAlign={textAlign}
          key={i}
        />
      ))}
    </View>
  );
};

export default PDFMarkdown;

const MarkdownBlock = ({
  token,
  scale,
  textAlign,
}: {
  token: Token;
  scale: number;
  textAlign: AlignTextType;
}) => {
  const gStyle = { textAlign: getTextAlign(textAlign) };

  switch (token.type) {
    case 'paragraph':
      return (
        <Text
          style={{ ...styles.p, ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
        >
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Text>
      );

    case 'heading':
      const hIndex = Math.min(3, Math.max(1, token.depth || 1));
      const HeadingTag = 'h' + hIndex;
      const headerFontSize = HEADING_SIZE[hIndex - 1];
      return (
        <Text
          style={{
            ...styles[HeadingTag],
            ...gStyle,
            fontSize: scale * headerFontSize,
          }}
        >
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Text>
      );

    case 'list':
      if (!token.items) throw new Error("Can't iterate through null items");
      const renderedList = token.items.map((item, i) => (
        <Text
          style={{ ...styles.li, ...gStyle, fontSize: PARAGRAPH_SIZE * scale }}
          key={i}
        >
          {token.ordered ? (token.depth || 1) + i + '.' : '-'}
          {'  '}
          <MarkdownLine tokens={item.tokens} scale={scale} />
        </Text>
      ));
      return <View style={{ ...gStyle, ...styles.list }}>{renderedList}</View>;

    case 'blockquote':
      const blockquoteToken = token.tokens && token.tokens[0];
      return (
        <View
          style={{
            ...styles.blockquote,
            ...gStyle,
            fontSize: PARAGRAPH_SIZE * scale,
          }}
        >
          {blockquoteToken && (
            <MarkdownBlock
              scale={scale}
              token={blockquoteToken}
              textAlign={textAlign}
            />
          )}
        </View>
      );

    case 'hr':
      return <View style={styles.hr} />;

    case 'code':
      return (
        <Text
          style={{ ...styles.code, ...gStyle, fontSize: CODE_SIZE * scale }}
        >
          {token.raw}
        </Text>
      );

    case 'space':
      return <View style={styles.br} />;

    default:
      return (
        <Text
          style={{ ...styles.code, ...gStyle, fontSize: CODE_SIZE * scale }}
        >
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Text>
      );
  }
};

const MarkdownLine = ({ tokens, scale }: { tokens?: Token[]; scale: number }) =>
  !tokens ? null : (
    <Text>
      {tokens.map((token, i) => (
        <MarkdownLineToken token={token} key={i} scale={scale} />
      ))}
    </Text>
  );

const MarkdownLineToken = ({
  token,
  scale,
}: {
  token: Token;
  scale: number;
}) => {
  switch (token.type) {
    case 'text':
      return <Text>{token.raw}</Text>;
    case 'strong':
      return (
        <Text style={styles.strong}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Text>
      );
    case 'em':
      return (
        <Text style={styles.em}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Text>
      );

    case 'link':
      return (
        <Link style={styles.link} src={token.href || ''}>
          <MarkdownLine tokens={token.tokens} scale={scale} />
        </Link>
      );

    case 'codespan':
      return (
        <Text style={{ ...styles.codespan, fontSize: CODE_SIZE * scale }}>
          {token.raw}
        </Text>
      );

    default:
      return <Text>{token.raw}</Text>;
  }
};

interface Token {
  type: string;
  depth?: number;
  items?: { tokens: Token[] }[];
  raw: string;
  text: string;
  href?: string;
  tokens?: Token[];
  ordered?: boolean;
  start?: number;
}

const styles = StyleSheet.create({
  container: { color: '#111' },
  p: { marginBottom: 16, color: '#888', fontWeight: 'normal' },
  h1: { fontWeight: 'bold', marginBottom: 8 },
  h2: { fontWeight: 'normal', marginBottom: 8 },
  h3: { fontWeight: 'light', marginBottom: 8 },
  list: { marginBottom: 16, paddingLeft: 16, color: '#888' },
  li: {},
  blockquote: { marginBottom: 16, marginLeft: 16 },
  hr: { height: 1, marginTop: 16, marginBottom: 16, backgroundColor: '#111' },
  code: { color: '#e83e8c' },
  br: { height: 20 },
  strong: { fontWeight: 'bold' },
  em: { fontStyle: 'italic' },
  link: { color: '#007bff' },
  codespan: { color: '#e83e8c' },
});

const HEADING_SIZE = [48, 44, 26];
const PARAGRAPH_SIZE = 14;
const CODE_SIZE = 12;
