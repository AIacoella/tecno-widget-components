import React from 'react';
import { DEFAULT_LEFT_ALIGN, getTextAlign, OptionType } from '../../utils';
import styles from './WidgetHeader.module.css';

export const WIDGET_TITLE_SIZE = 24;

interface WidgetHeaderProps {
  title?: string;
  scale: number;
  titleColor?: string;
  titleBackgroundColor?: string;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

const WidgetHeader: React.FunctionComponent<WidgetHeaderProps> = ({
  title,
  scale = 1,
  titleColor = '#212224',
  titleBackgroundColor,
  titleAlign = DEFAULT_LEFT_ALIGN,
}) => {
  return (
    <div
      className={styles.container}
      style={{ backgroundColor: titleBackgroundColor }}
    >
      <h3
        className={styles.header}
        style={{
          fontSize: scale * WIDGET_TITLE_SIZE,
          color: titleColor,
          textAlign: getTextAlign(titleAlign),
        }}
      >
        {title}
      </h3>
    </div>
  );
};

export default WidgetHeader;
