import React from 'react';
import styles from './SnapshotContainer.module.css';
import logo from './logo-tecno-dark.svg';
import moment from 'moment';
import { getMomentTF } from '../../utils';
import { WidgetProps } from '../../Widget';

interface TecnoSnapshotContainerProps extends WidgetProps {
  title?: string;
}

const TecnoSnapshotContainer: React.FunctionComponent<TecnoSnapshotContainerProps> = ({
  title,
  children,
  globalParameters = {},
}) => {
  const { timezone } = globalParameters;
  console.log(timezone);
  return (
    <div className={styles.container}>
      {title && (
        <div className={styles.header}>
          <h4>{title}</h4>
        </div>
      )}
      <div className={styles.content}>{children}</div>
      <div className={styles.footer}>
        <div className={styles.tecnoFooterText}>
          <span>
            Kontrolon ({getMomentTF(undefined, timezone).format('YYYY')}
            ). {getMomentTF(undefined, timezone).format('DD/MM/YYYY')}
          </span>
        </div>
        <img src={logo} alt="Tecno" />
      </div>
    </div>
  );
};

export default TecnoSnapshotContainer;
