import React from 'react';
import { View, Text, StyleSheet } from '@react-pdf/renderer';
import { HeaderProps, HEADER_FONT_SIZE } from './Header';
import WidgetContainerPDF from '../../components/WidgetContainerPDF/WidgetContainerPDF';
import { DEFAULT_ALIGN, getTextAlign, hex } from '../../utils';

interface HeaderPDFProps extends HeaderProps {}

export default function HeaderPDF({
  content,
  scale = 1,
  border,
  margin,
  padding,
  color,
  backgroundColor,
  textAlign = DEFAULT_ALIGN,
}: HeaderPDFProps) {
  return (
    <WidgetContainerPDF
      style={styles.container}
      wrap={false}
      scale={scale}
      border={border}
      margin={margin}
      padding={padding}
      backgroundColor={backgroundColor}
    >
      <Text
        style={{
          fontSize: HEADER_FONT_SIZE * scale,
          color: hex(color),
          textAlign: getTextAlign(textAlign),
        }}
      >
        {content}
      </Text>
    </WidgetContainerPDF>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    fontSize: 25,
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#ff8b49',
  },
});
