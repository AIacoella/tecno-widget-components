import React from 'react';
import styles from './Header.module.css';

import { WidgetContainer, WidgetProps } from '../../Widget';
import {
  DEFAULT_ALIGN,
  getFlexOrientation,
  getTextAlign,
  hex,
  OptionType,
} from '../../utils';
import sanitizeHtml from 'sanitize-html';

export interface HeaderProps extends WidgetProps {
  content: string;
  color?: string;
  textAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  renderHtml?: boolean;
}

export const HEADER_FONT_SIZE = 24;

const Header: React.FunctionComponent<HeaderProps> = ({
  rss,
  content,
  scale = 1,
  margin,
  padding,
  border,
  backgroundColor,
  color = 'ff8b49',
  textAlign = DEFAULT_ALIGN,
  renderHtml = false,
}) => {
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      backgroundColor={backgroundColor}
    >
      <div
        className={styles.container}
        style={{ alignItems: getFlexOrientation(textAlign) }}
      >
        {!renderHtml ? (
          <h2
            className={styles.header}
            style={{
              fontSize: HEADER_FONT_SIZE * scale,
              color: hex(color),
              textAlign: getTextAlign(textAlign),
            }}
          >
            {content}
          </h2>
        ) : (
          <div
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(content, SANITIZE_OPTIONS),
            }}
          />
        )}
      </div>
    </WidgetContainer>
  );
};

export default Header;

const allowGenericAttributes = (attributes: string[] = []) => {
  const tags = [
    'address',
    'article',
    'aside',
    'footer',
    'header',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'hgroup',
    'main',
    'nav',
    'section',
    'blockquote',
    'dd',
    'div',
    'dl',
    'dt',
    'figcaption',
    'figure',
    'hr',
    'li',
    'main',
    'ol',
    'p',
    'pre',
    'ul',
    'a',
    'abbr',
    'b',
    'bdi',
    'bdo',
    'br',
    'cite',
    'code',
    'data',
    'dfn',
    'em',
    'i',
    'kbd',
    'mark',
    'q',
    'rb',
    'rp',
    'rt',
    'rtc',
    'ruby',
    's',
    'samp',
    'small',
    'span',
    'strong',
    'sub',
    'sup',
    'time',
    'u',
    'var',
    'wbr',
    'caption',
    'col',
    'colgroup',
    'table',
    'tbody',
    'td',
    'tfoot',
    'th',
    'thead',
    'tr',
  ];
  const ret: { [key: string]: string[] } = {};
  tags.forEach(t => {
    ret[t] = attributes;
  });
  return ret;
};

const genericAttributes = allowGenericAttributes(['style']);

genericAttributes.a = ['href', 'name', 'target', 'style'];
genericAttributes.img = ['src', 'style'];

const SANITIZE_OPTIONS = {
  allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img']),
  allowedAttributes: genericAttributes,
};
