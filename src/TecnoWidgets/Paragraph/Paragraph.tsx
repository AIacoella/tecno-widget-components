import React from 'react';
import { WidgetContainer, WidgetProps } from '../../Widget';
import HtmlMarkdown from '../../components/HtmlMarkdown/HtmlMarkdown';
import { DEFAULT_LEFT_ALIGN, OptionType } from '../../utils';

export const FONT_SIZE = 14;

export interface ParagraphProps extends WidgetProps {
  content: string;
  className?: string;
  textAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

const Paragraph: React.FunctionComponent<ParagraphProps> = ({
  rss,
  content,
  scale = 1,
  margin,
  padding,
  border,
  className,
  textAlign = DEFAULT_LEFT_ALIGN,
  backgroundColor,
}) => {
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      backgroundColor={backgroundColor}
    >
      <HtmlMarkdown
        scale={scale}
        className={className}
        source={content}
        textAlign={textAlign}
      />
    </WidgetContainer>
  );
};

export default Paragraph;
