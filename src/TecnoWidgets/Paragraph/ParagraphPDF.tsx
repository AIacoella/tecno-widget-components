import React from 'react';
import { Text, StyleSheet } from '@react-pdf/renderer';
import { ParagraphProps, FONT_SIZE } from './Paragraph';
import WidgetContainerPDF from '../../components/WidgetContainerPDF/WidgetContainerPDF';
import PDFMarkdown from '../../components/PDFMarkdown/PDFMarkdown';
import { DEFAULT_LEFT_ALIGN } from '../../utils';

interface ParagraphPDFProps extends ParagraphProps {}

export default function ParagraphPDF({
  content = '',
  scale = 1,
  border,
  margin,
  padding,
  textAlign = DEFAULT_LEFT_ALIGN,
  backgroundColor,
}: ParagraphPDFProps) {
  return (
    <WidgetContainerPDF
      style={styles.container}
      wrap={true}
      scale={scale}
      border={border}
      margin={margin}
      padding={padding}
      backgroundColor={backgroundColor}
    >
      <PDFMarkdown source={content} scale={scale} textAlign={textAlign} />
    </WidgetContainerPDF>
  );
}

const styles = StyleSheet.create({
  container: {
    fontSize: 14,
    fontWeight: 300,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    color: '#888888',
  },
});
