import React from 'react';
import { OptionType } from 'utils';
import { WidgetContainer, WidgetProps } from '../../Widget';
import styles from './Image.module.css';

export interface ImageProps extends WidgetProps {
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  url?: string;
  objectFit?: { value: 'cover' | 'contain' | 'fill'; label: string };
  titleColor?: string;
  titleBackgroundColor?: string;
}

export const LABEL_SIZE = 13;
export const AXIS_LABEL_SIZE = 14;

const Image: React.FunctionComponent<ImageProps> = ({
  rss,
  title,
  titleScale,
  url,
  margin,
  padding,
  border,
  objectFit = { value: 'cover', label: '' },
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  titleAlign,
}) => {
  return (
    <WidgetContainer
      asyncRender={true}
      rss={rss}
      scale={1}
      title={title}
      titleScale={titleScale}
      margin={margin}
      padding={padding}
      border={border}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      titleAlign={titleAlign}
    >
      {({ hasRendered }: { hasRendered: () => void }) => (
        <img
          src={url}
          alt="Can't render Image"
          className={styles.img}
          onLoad={hasRendered}
          onError={hasRendered}
          style={{ objectFit: objectFit.value }}
          draggable="false"
        />
      )}
    </WidgetContainer>
  );
};

export default Image;
