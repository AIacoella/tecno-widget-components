import React from 'react';
import styles from './Display.module.css';

import { WidgetContainer, WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
import {
  DisplayComponent,
  FormatIntervalType,
  VALUE_FORMATS,
} from './DisplayComponent';

export interface DisplayProps extends WidgetProps {
  data?: number | string;
  prefix?: string;
  suffix?: string;
  label?: string;
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  decimalLimit?: false | number;
  color?: string;
  valueFormat?: { value: string };
  emptyValue?: string;
  titleColor?: string;
  titleBackgroundColor?: string;
  formatIntervals?: FormatIntervalType[];
}

const Display: React.FunctionComponent<DisplayProps> = ({
  rss,
  data,
  prefix,
  suffix,
  label,
  title,
  titleScale,
  scale = 1,
  margin,
  padding,
  border,
  decimalLimit,
  backgroundColor,
  color = '212224',
  valueFormat = { value: VALUE_FORMATS.NUMBER },
  emptyValue = '0',
  titleBackgroundColor,
  titleColor,
  formatIntervals = [],
  titleAlign,
}) => {
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      title={title}
      titleScale={titleScale}
      margin={margin}
      padding={padding}
      border={border}
      backgroundColor={backgroundColor}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      titleAlign={titleAlign}
    >
      <div className={styles.container}>
        <DisplayComponent
          data={data}
          prefix={prefix}
          suffix={suffix}
          label={label}
          decimalLimit={decimalLimit}
          color={color}
          valueFormat={valueFormat}
          emptyValue={emptyValue}
          formatIntervals={formatIntervals}
          scale={scale}
        />
      </div>
    </WidgetContainer>
  );
};

export default Display;
