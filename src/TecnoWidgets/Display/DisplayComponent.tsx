import React from 'react';
import { hex, limitDecimals, numberWithCommas } from '../../utils';
import styles from './Display.module.css';
import classnames from 'classnames';

export type FormatIntervalType = { bound: number; color: string };

export interface DisplayComponentProps {
  data?: number | string;
  prefix?: string;
  suffix?: string;
  label?: string;
  decimalLimit?: false | number;
  color?: string;
  valueFormat?: { value: string };
  emptyValue?: string;
  formatIntervals?: FormatIntervalType[];
  scale?: number;
  className?: string;
  style?: React.CSSProperties;
}

export const DISPLAY_FONT_SIZE = 24;
export const DISPLAY_LABEL_SIZE = 14;

export const DisplayComponent: React.FunctionComponent<DisplayComponentProps> = ({
  data,
  prefix,
  suffix,
  label,
  decimalLimit,
  color = '212224',
  valueFormat = { value: VALUE_FORMATS.NUMBER },
  emptyValue,
  formatIntervals = [],
  scale = 1,
  className,
  style,
}) => {
  const value =
    typeof data === 'number'
      ? formatValue(data, valueFormat, decimalLimit)
      : emptyValue;
  const format = getFormat(data, formatIntervals);

  return (
    <div
      className={classnames(styles.contentContainer, className)}
      style={style}
    >
      <h3
        className={styles.value}
        style={{
          fontSize: scale * DISPLAY_FONT_SIZE,
          color: format.color || hex(color),
        }}
      >
        {`${prefix ? prefix : ''}`}
        {value}
        {`${suffix ? suffix : ''}`}
      </h3>
      {label && (
        <span
          className={styles.label}
          style={{ fontSize: scale * DISPLAY_LABEL_SIZE }}
        >
          {label}
        </span>
      )}
    </div>
  );
};

export const VALUE_FORMATS = {
  NUMBER: 'NUMBER',
  HOURS_FULL: 'HOUR_HH:mm:ss',
  HOURS: 'HOUR_HH:mm',
  MINUTES_FULL: 'MINUTES_HH:mm:ss',
  MINUTES: 'MINUTES_HH:mm',
  SECONDS_FULL: 'SECONDS_HH:mm:ss',
  SECONDS: 'SECONDS_HH:mm',
};

const formatValue = (
  data: number | string,
  valueFormat: { value: string },
  decimalLimit?: number | false
) => {
  let value = Number(data);
  if (
    valueFormat.value === VALUE_FORMATS.HOURS ||
    valueFormat.value === VALUE_FORMATS.HOURS_FULL
  ) {
    const HH = Math.trunc(value);
    const mm = Math.trunc((value - HH) * 60);
    const ss = Math.trunc(((value - HH) * 60 - mm) * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.HOURS_FULL;
    return timeString(HH, mm, ss, isFull);
  } else if (
    valueFormat.value === VALUE_FORMATS.MINUTES ||
    valueFormat.value === VALUE_FORMATS.MINUTES_FULL
  ) {
    const HH = Math.trunc(value / 60);
    const mm = Math.trunc(value - HH * 60);
    const ss = Math.trunc((value - HH * 60 - mm) * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.MINUTES_FULL;
    return timeString(HH, mm, ss, isFull);
  } else if (
    valueFormat.value === VALUE_FORMATS.SECONDS ||
    valueFormat.value === VALUE_FORMATS.SECONDS_FULL
  ) {
    const HH = Math.trunc(value / (60 * 60));
    const mm = Math.trunc((value - HH * (60 * 60)) / 60);
    const ss = Math.trunc(value - HH * (60 * 60) - mm * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.SECONDS_FULL;
    return timeString(HH, mm, ss, isFull);
  } else return numberWithCommas(limitDecimals(value, decimalLimit));
};

//To double digits: 4 => 04
const dd = (n: number) => ('00' + n).slice(-2);

const timeString = (HH: number, mm: number, ss: number, isFull?: boolean) =>
  isFull ? `${HH}:${dd(mm)}:${dd(ss)}` : `${dd(HH)}:${dd(mm)}`;

const getFormat = (
  data: number | string | undefined,
  sections: FormatIntervalType[]
): { color?: string } => {
  if (typeof data !== 'number' || sections.length === 0) return {};
  const interval =
    sections.find(i => data < i.bound) || sections[sections.length - 1];
  return { color: interval.color };
};
