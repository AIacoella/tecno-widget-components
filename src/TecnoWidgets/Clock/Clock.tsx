import React, { useState, useEffect } from 'react';
import styles from './Clock.module.css';
import { WidgetContainer, WidgetProps } from '../../Widget';
//@ts-ignore
import ReactClock from 'react-clock';
import { SizeMe } from 'react-sizeme';
import { nowDateTF, OptionType } from '../../utils';

export const FONT_SIZE = 14;

export interface ClockProps extends WidgetProps {
  title?: string;
  titleScale?: number;
  titleColor?: string;
  titleBackgroundColor?: string;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

const Clock: React.FunctionComponent<ClockProps> = ({
  rss,
  title,
  titleScale,
  scale = 1,
  margin,
  padding,
  border,
  globalParameters = {},
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  titleAlign,
}) => {
  const { timezone } = globalParameters;
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      title={title}
      titleAlign={titleAlign}
      titleScale={titleScale}
      margin={margin}
      padding={padding}
      border={border}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
    >
      <SizeMe monitorHeight refreshRate={128}>
        {({ size }) => <ClockOperator size={size} timezone={timezone} />}
      </SizeMe>
    </WidgetContainer>
  );
};

export default Clock;

const ClockOperator: React.FunctionComponent<{
  size: { width: number | null; height: number | null };
  timezone?: string;
}> = ({ size, timezone }) => {
  const [value, setValue] = useState(nowDateTF(timezone));

  useEffect(() => {
    const interval = setInterval(() => setValue(nowDateTF(timezone)), 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className={styles.container}>
      <ReactClock
        value={value}
        size={Math.min(size.height || 0, size.width || 0)}
        className={styles.clock}
      />
    </div>
  );
};
