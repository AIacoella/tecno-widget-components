import React from 'react';
import styles from './Panel.module.css';
import { WidgetContainer, WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
import {
  DisplayComponent,
  DisplayComponentProps,
} from '../Display/DisplayComponent';
import { GaugeComponent, GaugeComponentProps } from '../Gauge/GaugeComponent';

export const FONT_SIZE = 14;

interface PanelSource {
  type: 'DISPLAY' | 'GAUGE';
  x: number;
  y: number;
  parameters: DisplayComponentProps | GaugeComponentProps;
}

type DataType = number | string;

export interface PanelProps extends WidgetProps {
  data?: DataType[];
  sources: PanelSource[];
  url?: string;
  objectFit?: { value: 'cover' | 'contain' | 'fill'; label: string };
  title?: string;
  titleScale?: number;
  titleColor?: string;
  titleBackgroundColor?: string;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

const Panel: React.FunctionComponent<PanelProps> = ({
  data = [],
  sources = [],
  rss,
  scale = 1,
  margin,
  padding,
  border,
  backgroundColor,
  title,
  titleScale,
  titleBackgroundColor,
  titleColor,
  titleAlign,
  url,
  objectFit,
}) => {
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      title={title}
      titleAlign={titleAlign}
      titleScale={titleScale}
      margin={margin}
      padding={padding}
      border={border}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
    >
      <div className={styles.container}>
        <img src={url} style={{ objectFit: objectFit?.value }} />
        {Array.isArray(data) &&
          data.map((value, i) => (
            <PanelComponent
              key={`panelComponent-${i}-${data.length}`}
              value={value}
              {...sources[i]}
            />
          ))}
      </div>
    </WidgetContainer>
  );
};

interface PanelComponentProps extends PanelSource {
  value: DataType;
}

const PANEL_COMPONET_HEIGHT = 30;
const PANEL_GAUGE_SIZE = 140;

const PanelComponent: React.FunctionComponent<PanelComponentProps> = ({
  value,
  type,
  x,
  y,
  parameters = {},
}) => {
  const { backgroundColor } = parameters as any;
  const gaugeSize = PANEL_GAUGE_SIZE * (parameters.scale || 1);
  const styleContainer = {
    top: `${y}%`,
    left: `${x}%`,
    height:
      type === 'DISPLAY'
        ? PANEL_COMPONET_HEIGHT * (parameters.scale || 1)
        : gaugeSize / 2,
  };
  const styleContent = { backgroundColor };

  return (
    <div className={styles.panelComponentContainer} style={styleContainer}>
      <div className={styles.panelComponent} style={styleContent}>
        {type === 'DISPLAY' ? (
          <DisplayComponent data={value} {...parameters} />
        ) : (
          <div style={{ width: gaugeSize, height: gaugeSize / 2 }}>
            <GaugeComponent data={value} {...parameters} />
          </div>
        )}
      </div>
    </div>
  );
};

export default Panel;
