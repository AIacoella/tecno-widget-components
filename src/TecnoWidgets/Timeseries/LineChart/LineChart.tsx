import React, { useMemo } from 'react';
import { WidgetContainer } from '../../../Widget';
import moment from 'moment-timezone';
import { LineChart as ReLineChart, Line } from 'recharts';
import {
  DEFAULT_COLORS,
  dataLimiter,
  getDateTimeFormat,
  safetyLimit,
  interpolator,
} from '../../../utils';
import Chart, { SAFETY_LIMIT_WARNING } from '../Chart';
import { TimeSeriesProps, SingelChartData, DomainType } from '../types';
import _ from 'lodash';

export interface LineChartProps extends TimeSeriesProps<SingelChartData> {
  hideLineDot?: boolean;
  yDomain?: DomainType;
}

const LineChart: React.FunctionComponent<LineChartProps> = ({
  rss,
  data: dataBatch,
  title,
  titleScale,
  hideXAxis,
  hideYAxis,
  xAxisLabel,
  yAxisLabel,
  showLegend,
  scale = 1,
  margin,
  padding,
  border,
  globalParameters = {},
  decimalLimit,
  hideLineDot,
  dateTimeFormat = 'DATETIME',
  showBrush,
  setBrushValue,
  brushValue,
  setIsDraggable,
  yDomain,
  dataLimit,
  referenceLines,
  adjustDomain,
  timeframe: customTF,
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  safetyDataLimit,
  interpolate,
}) => {
  let { data = [], sources = [] } = dataBatch;
  let safetyDataLimitEnabeld = false;
  if (safetyDataLimit && !dataLimit) {
    const safetyData = safetyLimit(data, sources.length);
    safetyDataLimitEnabeld = safetyData.length < data.length;
    data = safetyData;
  }
  if (dataLimit) data = dataLimiter(data, dataLimit);
  const { timezone, timeframe } = globalParameters;
  const isDayInterval =
    data.length > 0
      ? moment(data[0].timestamp)
          .add(25, 'hours')
          .isAfter(moment(data[data.length - 1].timestamp))
      : false;
  const autoDateFormat = isDayInterval ? 'HH:mm' : 'DD/MM';
  const dateFormat = getDateTimeFormat(dateTimeFormat);

  const hasData = data.length > 0;

  const interpolatedData = useMemo(() => {
    if (interpolate)
      return interpolator(
        _.cloneDeep(data),
        sources.map(s => s.id)
      );
    else return data;
  }, [dataBatch, interpolate]);

  return (
    <WidgetContainer
      rss={rss}
      title={title}
      titleScale={titleScale}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      style={{ overflowX: 'hidden' }}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      warning={safetyDataLimitEnabeld ? SAFETY_LIMIT_WARNING : undefined}
    >
      <Chart
        ChartComponent={ReLineChart}
        data={interpolatedData}
        hideXAxis={hideXAxis}
        hideYAxis={hideYAxis}
        xAxisLabel={xAxisLabel}
        yAxisLabel={yAxisLabel}
        decimalLimit={decimalLimit}
        scale={scale}
        timezone={timezone}
        autoDateFormat={autoDateFormat}
        showBrush={showBrush}
        rss={rss}
        dateFormat={dateFormat}
        hasData={hasData}
        brushValue={brushValue}
        setBrushValue={setBrushValue}
        setIsDraggable={setIsDraggable}
        showLegend={showLegend}
        yDomain={yDomain}
        referenceLines={referenceLines}
        timeframe={customTF || timeframe}
        adjustDomain={adjustDomain}
      >
        {sources.map((source, i) => (
          <Line
            yAxisId={0}
            key={source.id}
            dataKey={source.id}
            type="monotone"
            strokeWidth={3 * scale}
            stroke={source.color || DEFAULT_COLORS[i % DEFAULT_COLORS.length]}
            isAnimationActive={!rss}
            name={source.label || source.id}
            dot={hideLineDot ? false : undefined}
          />
        ))}
      </Chart>
    </WidgetContainer>
  );
};

export default LineChart;
