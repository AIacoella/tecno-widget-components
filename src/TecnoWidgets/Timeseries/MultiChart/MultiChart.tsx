import React, { useMemo } from 'react';
import { WidgetContainer } from '../../../Widget';
import moment from 'moment-timezone';
import { ComposedChart, Line, Bar, Area } from 'recharts';
import {
  DEFAULT_COLORS,
  dataLimiter,
  getDateTimeFormat,
  interpolator,
  safetyLimit,
} from '../../../utils';
import Chart, { SAFETY_LIMIT_WARNING } from '../Chart';
import {
  TimeSeriesProps,
  MultiChartData,
  MultiSourceInterface,
  AxisType,
} from '../types';
import _ from 'lodash';

export interface MultiChartProps extends TimeSeriesProps<MultiChartData> {
  hideLineDot?: boolean;
  yAxis?: AxisType[];
}

const MultiChart: React.FunctionComponent<MultiChartProps> = ({
  rss,
  data: dataBatch,
  title,
  titleScale,
  hideXAxis,
  hideYAxis,
  xAxisLabel,
  yAxisLabel,
  showLegend,
  scale = 1,
  margin,
  padding,
  border,
  globalParameters = {},
  decimalLimit,
  hideLineDot,
  dateTimeFormat = 'DATETIME',
  showBrush,
  setBrushValue,
  brushValue,
  setIsDraggable,
  dataLimit,
  yAxis,
  interpolate,
  referenceLines,
  adjustDomain,
  timeframe: customTF,
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  safetyDataLimit,
  titleAlign,
}) => {
  let { data = [], sources = [] } = dataBatch;
  let safetyDataLimitEnabeld = false;
  if (safetyDataLimit && !dataLimit) {
    const safetyData = safetyLimit(data, sources.length);
    safetyDataLimitEnabeld = safetyData.length < data.length;
    data = safetyData;
  }
  if (dataLimit) data = dataLimiter(data, dataLimit);
  const { timezone, timeframe } = globalParameters;
  const isDayInterval =
    data.length > 0
      ? moment(data[0].timestamp)
          .add(25, 'hours')
          .isAfter(moment(data[data.length - 1].timestamp))
      : false;
  const autoDateFormat = isDayInterval ? 'HH:mm' : 'DD/MM';
  const dateFormat = getDateTimeFormat(dateTimeFormat);

  const hasData = data.length > 0;

  const interpolatedData = useMemo(() => {
    if (interpolate)
      return interpolator(
        _.cloneDeep(data),
        sources.map(s => s.id)
      );
    else return data;
  }, [dataBatch, interpolate]);

  return (
    <WidgetContainer
      rss={rss}
      title={title}
      titleScale={titleScale}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      style={{ overflowX: 'hidden' }}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      warning={safetyDataLimitEnabeld ? SAFETY_LIMIT_WARNING : undefined}
      titleAlign={titleAlign}
    >
      <Chart
        ChartComponent={ComposedChart}
        data={interpolatedData}
        hideXAxis={hideXAxis}
        hideYAxis={hideYAxis}
        xAxisLabel={xAxisLabel}
        yAxisLabel={yAxisLabel}
        decimalLimit={decimalLimit}
        scale={scale}
        timezone={timezone}
        autoDateFormat={autoDateFormat}
        showBrush={showBrush}
        rss={rss}
        dateFormat={dateFormat}
        hasData={hasData}
        brushValue={brushValue}
        setBrushValue={setBrushValue}
        setIsDraggable={setIsDraggable}
        showLegend={showLegend}
        yAxis={yAxis}
        referenceLines={referenceLines}
        timeframe={customTF || timeframe}
        adjustDomain={adjustDomain}
      >
        {sources.map((source, i) =>
          MultiElement({ source, index: i, rss, hideLineDot, scale })
        )}
      </Chart>
    </WidgetContainer>
  );
};

export default MultiChart;

interface MultiElementProps {
  source: MultiSourceInterface;
  index: number;
  rss?: boolean;
  hideLineDot?: boolean;
  scale: number;
}

const MultiElement = ({
  source,
  index,
  rss,
  scale,
  hideLineDot,
}: MultiElementProps) => {
  const id = source.id;
  const color = source.color || DEFAULT_COLORS[index % DEFAULT_COLORS.length];
  const name = source.label || id;
  console.log(source);
  const axisId = source.axis || 0;
  console.log('Axis ID:', axisId);

  if (source.chartType === 'BAR')
    return (
      <Bar
        yAxisId={axisId}
        key={id}
        dataKey={id}
        fill={color}
        isAnimationActive={!rss}
        name={name}
        stackId={source.stackBar ? STACK_ID : undefined}
      />
    );
  else if (source.chartType === 'AREA')
    return (
      <Area
        yAxisId={axisId}
        key={id}
        type="monotone"
        dataKey={id}
        fill={color}
        stroke={color}
        isAnimationActive={!rss}
      />
    );
  else
    return (
      <Line
        yAxisId={axisId}
        key={id}
        dataKey={id}
        type="monotone"
        strokeWidth={3 * scale}
        stroke={color}
        isAnimationActive={!rss}
        name={name}
        dot={hideLineDot ? false : undefined}
      />
    );
};

const STACK_ID = 'STACKED';
