import { OptionType, TimeframeInterface } from 'utils';
import { WidgetProps } from '../../Widget';

export interface SourceInterface {
  id: string;
  color?: string;
  label?: string;
}

export interface MultiSourceInterface {
  id: string;
  chartType: 'LINE' | 'BAR' | 'AREA';
  color?: string;
  label?: string;
  axis?: number;
  stackBar?: boolean;
}

export interface SingelChartData {
  sources: SourceInterface[];
  data: any[];
}

export interface MultiChartData {
  sources: MultiSourceInterface[];
  data: any[];
}

export interface TimeSeriesProps<DataType> extends WidgetProps {
  data: DataType;
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  hideXAxis?: boolean;
  hideYAxis?: boolean;
  xAxisLabel?: string;
  yAxisLabel?: string;
  showLegend?: boolean;
  decimalLimit?: false | number;
  dateTimeFormat?: 'DATE' | 'TIME' | 'DATETIME';
  showBrush?: boolean;
  setBrushValue?: (pos: { startIndex: number; endIndex: number }) => {};
  brushValue?: { startIndex: number; endIndex: number };
  setIsDraggable?: (isDraggable: boolean) => {};
  dataLimit?: number;
  interpolate?: boolean;
  referenceLines?: ReferenceLineType[];
  adjustDomain?: boolean;
  timeframe?: TimeframeInterface;
  titleColor?: string;
  titleBackgroundColor?: string;
  safetyDataLimit?: boolean;
}

export type ReferenceLineType = {
  value: number;
  label: string;
  color: string;
  axis: number;
};

export type DomainType = { min: number | string; max: number | string };

export type AxisType = {
  orientation: 'left' | 'right';
  label?: string;
  domain?: DomainType;
  hideAxis?: boolean;
};
