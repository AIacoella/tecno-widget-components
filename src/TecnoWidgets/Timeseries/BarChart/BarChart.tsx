import React, { useMemo } from 'react';
import { WidgetContainer } from '../../../Widget';
import moment from 'moment-timezone';
import { BarChart as BarChartRecharts, Bar } from 'recharts';
import {
  dataLimiter,
  safetyLimit,
  DEFAULT_COLORS,
  getDateTimeFormat,
  interpolator,
} from '../../../utils';
import { DomainType, SingelChartData, TimeSeriesProps } from '../types';
import Chart, { SAFETY_LIMIT_WARNING } from '../Chart';
import _ from 'lodash';

export interface BarChartProps extends TimeSeriesProps<SingelChartData> {
  yDomain?: DomainType;
}

const BarChart: React.FunctionComponent<BarChartProps> = ({
  rss,
  data: dataBatch,
  title,
  titleScale,
  hideXAxis,
  hideYAxis,
  xAxisLabel,
  yAxisLabel,
  showLegend,
  scale = 1,
  margin,
  padding,
  border,
  decimalLimit,
  globalParameters = {},
  dateTimeFormat = 'DATETIME',
  showBrush,
  setBrushValue,
  brushValue,
  setIsDraggable,
  yDomain,
  dataLimit,
  referenceLines,
  adjustDomain,
  timeframe: customTF,
  titleColor,
  titleBackgroundColor,
  backgroundColor,
  safetyDataLimit,
  interpolate,
}) => {
  let { data = [], sources = [] } = dataBatch;
  let safetyDataLimitEnabeld = false;
  if (safetyDataLimit && !dataLimit) {
    const safetyData = safetyLimit(data, sources.length);
    safetyDataLimitEnabeld = safetyData.length < data.length;
    data = safetyData;
  } else if (dataLimit) data = dataLimiter(data, dataLimit);
  const { timezone, timeframe } = globalParameters;
  const isDayInterval =
    data.length > 0
      ? moment(data[0].timestamp)
          .add(25, 'hours')
          .isAfter(moment(data[data.length - 1].timestamp))
      : false;

  const autoDateFormat = isDayInterval ? 'HH:mm' : 'DD/MM';
  const dateFormat = getDateTimeFormat(dateTimeFormat);

  const hasData = data.length > 0;

  const interpolatedData = useMemo(() => {
    if (interpolate)
      return interpolator(
        _.cloneDeep(data),
        sources.map(s => s.id)
      );
    else return data;
  }, [dataBatch, interpolate]);

  return (
    <WidgetContainer
      rss={rss}
      title={title}
      titleScale={titleScale}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      style={{ overflowX: 'hidden' }}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      warning={safetyDataLimitEnabeld ? SAFETY_LIMIT_WARNING : undefined}
    >
      <Chart
        ChartComponent={BarChartRecharts}
        data={interpolatedData}
        hideXAxis={hideXAxis}
        hideYAxis={hideYAxis}
        xAxisLabel={xAxisLabel}
        yAxisLabel={yAxisLabel}
        decimalLimit={decimalLimit}
        scale={scale}
        timezone={timezone}
        autoDateFormat={autoDateFormat}
        showBrush={showBrush}
        rss={rss}
        dateFormat={dateFormat}
        hasData={hasData}
        brushValue={brushValue}
        setBrushValue={setBrushValue}
        setIsDraggable={setIsDraggable}
        showLegend={showLegend}
        yDomain={yDomain}
        referenceLines={referenceLines}
        timeframe={customTF || timeframe}
        adjustDomain={adjustDomain}
      >
        {sources.map((source, i) => (
          <Bar
            yAxisId={0}
            dataKey={source.id}
            fill={source.color || DEFAULT_COLORS[i % DEFAULT_COLORS.length]}
            isAnimationActive={!rss}
            name={source.label || source.id}
          />
        ))}
      </Chart>
    </WidgetContainer>
  );
};

export default BarChart;
