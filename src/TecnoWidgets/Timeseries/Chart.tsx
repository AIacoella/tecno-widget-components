import React, { useEffect, useRef, useState } from 'react';
import {
  AxisDomain,
  Brush,
  CartesianGrid,
  Label,
  Legend,
  ReferenceLine,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import {
  getMomentTF,
  getTimeframe,
  shortValue,
  TimeframeInterface,
} from '../../utils';
import { AxisType, DomainType, ReferenceLineType } from './types';
import _ from 'lodash';
import moment from 'moment';

export const LABEL_SIZE = 13;
export const AXIS_LABEL_SIZE = 14;

interface AxisProps {
  hideXAxis?: boolean;
  hideYAxis?: boolean;
  autoDateFormat: string;
  scale: number;
  timezone?: string;
  xAxisLabel?: string;
  yAxisLabel?: string;
  decimalLimit?: false | number;
}

interface ChartProps extends AxisProps {
  dateFormat: 'HH:mm' | 'DD/MM' | 'DD/MM HH:mm';
  hasData: boolean;
  setBrushValue?: (pos: { startIndex: number; endIndex: number }) => {};
  brushValue?: { startIndex: number; endIndex: number };
  setIsDraggable?: (isDraggable: boolean) => {};
  showLegend?: boolean;
  ChartComponent: any;
  data: any;
  yDomain?: { min: string | number; max: string | number };
  yAxis?: AxisType[];
  rss?: boolean;
  showBrush?: boolean;
  referenceLines?: ReferenceLineType[];
  timeframe?: TimeframeInterface;
  adjustDomain?: boolean;
}

const Chart: React.FunctionComponent<ChartProps> = ({
  hideXAxis,
  xAxisLabel,
  showBrush,
  autoDateFormat,
  scale,
  timezone,
  decimalLimit,
  dateFormat,
  hasData,
  setBrushValue,
  brushValue,
  setIsDraggable,
  showLegend,
  ChartComponent,
  data,
  hideYAxis,
  yAxisLabel,
  yDomain,
  yAxis = [defaultYAxis(yDomain, yAxisLabel, hideYAxis)],
  rss,
  referenceLines = [],
  timeframe,
  adjustDomain = false,
  children,
}) => {
  const isShowingBrush = !!showBrush && !rss;

  const containerRef = useRef<any>();
  const brushNode = getBrushNode(containerRef);
  const [uBrush, setUsingBrush] = useState(false);

  const onBrushStart = () => {
    setUsingBrush(true);
    setIsDraggable && setIsDraggable(false);
  };
  const onBrushEnd = () => {
    setUsingBrush(usingBrush => {
      usingBrush && setIsDraggable && setIsDraggable(true);
      return false;
    });
  };

  const removeListeners = () => {
    window.removeEventListener('mouseup', onBrushEnd);
    window.removeEventListener('touchend', onBrushEnd);
  };

  useEffect(() => {
    if (brushNode) {
      brushNode.addEventListener('mousedown', onBrushStart);
      brushNode.addEventListener('touchstart', onBrushStart);
      window.addEventListener('mouseup', onBrushEnd);
      window.addEventListener('touchend', onBrushEnd);
    } else {
      removeListeners();
    }
    return removeListeners;
  }, [brushNode]);

  const { from, to } = getTimeframe(timeframe, timezone);

  console.log('Timeframe, from, to', timeframe, from, to);
  console.log('Timezone: ', timezone);

  const xAxisHeight = 21 + (xAxisLabel ? 25 : 0);
  const refreshKey = showBrush + '';

  const xDomain = xDomainOverride(from, to, showBrush || adjustDomain);

  return (
    <ResponsiveContainer width="100%" height="100%" ref={containerRef}>
      <ChartComponent data={data} key={refreshKey}>
        <XAxis
          hide={hideXAxis}
          type="number"
          dataKey="timestamp"
          tickLine={true}
          axisLine={true}
          allowDecimals={false}
          height={xAxisHeight + (isShowingBrush ? 10 : 0)}
          domain={xDomain}
          allowDataOverflow
          tick={props => (
            <CustomTickXAxis
              {...props}
              dateFormat={autoDateFormat}
              scale={scale}
              timezone={timezone}
            />
          )}
          ticks={getXDomainTicks(data, xDomain, autoDateFormat)}
        >
          {xAxisLabel && (
            <Label
              value={xAxisLabel}
              position="bottom"
              offset={-10 * scale + (isShowingBrush ? -10 : 0)}
              fontSize={AXIS_LABEL_SIZE * scale}
            />
          )}
        </XAxis>
        {yAxis.map((axis, axisIndex) => {
          return (
            <YAxis
              key={axisIndex}
              yAxisId={axisIndex}
              tickLine={true}
              axisLine={true}
              orientation={axis.orientation || 'left'}
              width={(55 + (axis.label ? 20 : 0)) * scale}
              type="number"
              domain={yDomainOverride(axis.domain)}
              allowDataOverflow
              tick={props => (
                <CustomTickYAxis
                  {...props}
                  scale={scale}
                  decimalLimit={decimalLimit}
                  orientation={axis.orientation || 'left'}
                />
              )}
            >
              {axis.label && (
                <Label
                  value={axis.label}
                  angle={-90}
                  position={axis.orientation === 'right' ? 'right' : 'left'}
                  offset={(axis.orientation === 'right' ? -10 : -10) * scale}
                  style={{ textAnchor: 'middle' }}
                  fontSize={AXIS_LABEL_SIZE * scale}
                />
              )}
            </YAxis>
          );
        })}
        <Tooltip
          isAnimationActive={false}
          labelFormatter={timestampFormatter(dateFormat, timezone)}
          formatter={value => shortValue(Number(value), decimalLimit)}
        />
        {hasData && (
          <Brush
            dataKey="timestamp"
            onChange={setBrushValue}
            startIndex={showBrush ? brushValue?.startIndex : 0}
            endIndex={showBrush ? brushValue?.endIndex : data.length - 1}
            height={isShowingBrush ? 30 : -1}
            stroke="#8884d8"
            tickFormatter={timestampFormatter(dateFormat, timezone)}
          />
        )}
        {showLegend && <Legend verticalAlign="top" height={36 * scale} />}
        <CartesianGrid vertical={false} strokeDasharray={'5, 5'} />
        {referenceLines.map((line, i) => (
          <ReferenceLine
            key={'ReferenceLine-' + i}
            y={line.value}
            label={line.label}
            stroke={line.color}
            strokeDasharray="3 3"
            yAxisId={line.axis || 0}
          />
        ))}
        {children}
      </ChartComponent>
    </ResponsiveContainer>
  );
};

export default Chart;

export const timestampFormatter = (dateFormat: string, timezone?: string) => (
  timestamp: number | string
) => getMomentTF(timestamp, timezone).format(dateFormat);

const getBrushNode = (ref: React.MutableRefObject<any>) => {
  const nodeList =
    ref?.current?.container?.querySelectorAll('.recharts-brush') || [];
  if (nodeList.length !== 0) return nodeList[0];
  else return null;
};

export const CustomTickYAxis = ({
  x,
  y,
  payload,
  scale,
  decimalLimit,
  orientation,
}: any) => {
  const value = shortValue(payload.value, decimalLimit);
  const isRight = orientation === 'right';
  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={isRight ? 5 : -5}
        y={0}
        dy={0}
        textAnchor={isRight ? 'start' : 'end'}
        fill="#212224"
        fontSize={LABEL_SIZE * scale}
      >
        {value}
      </text>
    </g>
  );
};

export const CustomTickXAxis = ({
  x,
  y,
  payload,
  scale,
  timezone,
  dateFormat,
}: any) => {
  const date = getMomentTF(payload.value, timezone).format(dateFormat);

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={-5}
        y={0}
        dy={16}
        textAnchor="middle"
        fill="#212224"
        fontSize={LABEL_SIZE * scale}
      >
        {date}
      </text>
    </g>
  );
};

const yDomainOverride = (yDomain?: {
  min: string | number;
  max: string | number;
}): [AxisDomain, AxisDomain] | undefined =>
  !yDomain ? undefined : [Number(yDomain.min), Number(yDomain.max)];

const defaultYAxis = (
  yDomain?: DomainType,
  yAxisLabel?: string,
  hideYAxis?: boolean
): AxisType => ({
  orientation: 'left',
  hideAxis: hideYAxis,
  domain: yDomain,
  label: yAxisLabel,
});

const xDomainOverride = (
  from: number | undefined,
  to: number,
  adjustDomain: boolean
): [AxisDomain, AxisDomain] =>
  adjustDomain || from == null ? ['dataMin', 'dataMax'] : [from, to];

export const SAFETY_LIMIT_WARNING = 'Safety sub-sampling enabled';

const MANUAL_TICKS_THRESHOLD = 1000 * 60 * 60 * 24;
const MILLI_HOUR = 1000 * 60 * 60;

const getXDomainTicks = (
  data: any[],
  domain: [AxisDomain, AxisDomain],
  dateFormat: string
): number[] | undefined => {
  if (dateFormat !== 'HH:mm') return;
  const from = domain[0] === 'dataMin' ? data[0].timestamp : domain[0];
  const to =
    domain[1] === 'dataMax' ? data[data.length - 1].timestamp : domain[1];
  if (to - from > MANUAL_TICKS_THRESHOLD || to - from < MILLI_HOUR * 4) return;
  const ticks = [];
  const nTicks = 4;
  const tickOffset = (to - from) / (nTicks - 1);
  ticks.push(from);
  for (let i = 1; i < nTicks - 1; i++)
    ticks.push(Math.floor((from + i * tickOffset) / MILLI_HOUR) * MILLI_HOUR);
  ticks.push(to);
  console.log(ticks);
  return ticks;
};
