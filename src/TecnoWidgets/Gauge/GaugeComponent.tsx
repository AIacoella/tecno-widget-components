import React, { useState } from 'react';
import styles from './Gauge.module.css';
import { hex, limitDecimals, numberWithCommas, OptionType } from '../../utils';
import GaugeChart from 'react-gauge-chart';
import { SizeMe } from 'react-sizeme';
import classnames from 'classnames';

export type FormatIntervalType = { bound: number; color: string };

export interface GaugeComponentProps {
  data?: number | string;
  prefix?: string;
  suffix?: string;
  label?: string;
  decimalLimit?: false | number;
  color?: string;
  valueFormat?: { value: string };
  emptyValue?: string;
  sections?: { bound: number; color: string }[];
  scale?: number;
}

export const GAUGE_FONT_SIZE = 24;
export const GAUGE_LABEL_SIZE = 14;

const ARC_WIDTH = 0.2;

export const GaugeComponent: React.FunctionComponent<GaugeComponentProps> = ({
  data,
  prefix,
  suffix,
  label,
  decimalLimit,
  color = '212224',
  valueFormat = { value: VALUE_FORMATS.NUMBER },
  emptyValue = '0',
  sections = DEFAULT_SECTIONS,
  scale = 1,
}) => {
  const [id, setId] = useState(
    Math.round(Math.random() * Math.pow(10, 12)).toString()
  );
  let value: string, percentage: number;
  const maxValue = sections[sections.length - 1].bound;
  if (typeof data !== 'number' || typeof maxValue !== 'number') {
    value = emptyValue;
    percentage = 0;
  } else {
    value = formatValue(data, valueFormat, decimalLimit);
    percentage = Math.min(1, Math.max(0, data / maxValue));
  }
  return (
    <>
      <SizeMe monitorHeight refreshRate={128}>
        {({ size: { width, height } }) => {
          width = width || 0;
          height = height || 0;
          width = width > height * 2 ? height * 2 : width;
          return (
            <div
              className={styles.gaugeContainer}
              style={{ height: label ? 'calc(100% - 20px)' : '100%' }}
            >
              <GaugeChart
                id={`gauge-${id}`}
                nrOfLevels={sections.length}
                arcsLength={getArcsLength(sections)}
                cornerRadius={0}
                arcPadding={0}
                percent={percentage}
                textColor={color}
                formatTextValue={() =>
                  `${prefix ? prefix : ''}${value}${suffix ? suffix : ''}`
                }
                marginInPercent={0.03}
                style={{
                  height: width / 2,
                  width,
                  fontSize: GAUGE_FONT_SIZE * scale,
                }}
                className={styles.gauge}
                animate={false}
                arcWidth={ARC_WIDTH * scale}
              />
            </div>
          );
        }}
      </SizeMe>
      {label && (
        <div className={styles.labelContainer}>
          <span
            className={styles.label}
            style={{ fontSize: scale * GAUGE_LABEL_SIZE }}
          >
            {label}
          </span>
        </div>
      )}
    </>
  );
};

const VALUE_FORMATS = {
  NUMBER: 'NUMBER',
  HOURS_FULL: 'HOUR_HH:mm:ss',
  HOURS: 'HOUR_HH:mm',
  MINUTES_FULL: 'MINUTES_HH:mm:ss',
  MINUTES: 'MINUTES_HH:mm',
  SECONDS_FULL: 'SECONDS_HH:mm:ss',
  SECONDS: 'SECONDS_HH:mm',
};

const formatValue = (
  data: number | string,
  valueFormat: { value: string },
  decimalLimit?: number | false
) => {
  let value = Number(data);
  if (
    valueFormat.value === VALUE_FORMATS.HOURS ||
    valueFormat.value === VALUE_FORMATS.HOURS_FULL
  ) {
    const HH = Math.trunc(value);
    const mm = Math.trunc((value - HH) * 60);
    const ss = Math.trunc(((value - HH) * 60 - mm) * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.HOURS_FULL;
    return timeString(HH, mm, ss, isFull);
  } else if (
    valueFormat.value === VALUE_FORMATS.MINUTES ||
    valueFormat.value === VALUE_FORMATS.MINUTES_FULL
  ) {
    const HH = Math.trunc(value / 60);
    const mm = Math.trunc(value - HH * 60);
    const ss = Math.trunc((value - HH * 60 - mm) * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.MINUTES_FULL;
    return timeString(HH, mm, ss, isFull);
  } else if (
    valueFormat.value === VALUE_FORMATS.SECONDS ||
    valueFormat.value === VALUE_FORMATS.SECONDS_FULL
  ) {
    const HH = Math.trunc(value / (60 * 60));
    const mm = Math.trunc((value - HH * (60 * 60)) / 60);
    const ss = Math.trunc(value - HH * (60 * 60) - mm * 60);
    const isFull = valueFormat.value === VALUE_FORMATS.SECONDS_FULL;
    return timeString(HH, mm, ss, isFull);
  } else return numberWithCommas(limitDecimals(value, decimalLimit));
};

//To double digits: 4 => 04
const dd = (n: number) => ('00' + n).slice(-2);

const timeString = (HH: number, mm: number, ss: number, isFull?: boolean) =>
  isFull ? `${HH}:${dd(mm)}:${dd(ss)}` : `${dd(HH)}:${dd(mm)}`;

const DEFAULT_SECTIONS = [{ bound: 1, color: 'rgb(0, 255, 0)' }];

const getArcsLength = (sections: { bound: number }[]) => {
  const max = sections[sections.length - 1].bound;
  let traveled = 0;
  return sections.map(({ bound }) => {
    const interval = bound / max - traveled;
    traveled += interval;
    return interval;
  });
};
