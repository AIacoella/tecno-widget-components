import React from 'react';
import styles from './Gauge.module.css';

import { WidgetContainer, WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
import { GaugeComponent } from './GaugeComponent';

export interface GaugeProps extends WidgetProps {
  data?: number | string;
  prefix?: string;
  suffix?: string;
  label?: string;
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  decimalLimit?: false | number;
  color?: string;
  valueFormat?: { value: string };
  emptyValue?: string;
  titleColor?: string;
  titleBackgroundColor?: string;
  sections?: { bound: number; color: string }[];
}

const Gauge: React.FunctionComponent<GaugeProps> = ({
  rss,
  data,
  prefix,
  suffix,
  title,
  titleScale,
  scale = 1,
  label,
  margin,
  padding,
  border,
  decimalLimit,
  backgroundColor,
  color = '212224',
  valueFormat,
  emptyValue = '0',
  titleColor,
  titleBackgroundColor,
  sections,
  titleAlign,
}) => {
  return (
    <WidgetContainer
      rss={rss}
      scale={scale}
      title={title}
      titleScale={titleScale}
      margin={margin}
      padding={padding}
      border={border}
      backgroundColor={backgroundColor}
      titleColor={titleColor}
      titleBackgroundColor={titleBackgroundColor}
      titleAlign={titleAlign}
    >
      <GaugeComponent
        data={data}
        prefix={prefix}
        suffix={suffix}
        label={label}
        decimalLimit={decimalLimit}
        color={color}
        valueFormat={valueFormat}
        emptyValue={emptyValue}
        sections={sections}
        scale={scale}
      />
    </WidgetContainer>
  );
};

export default Gauge;
