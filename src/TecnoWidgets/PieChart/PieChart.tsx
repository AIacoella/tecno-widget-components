import React from 'react';
import { WidgetContainer, WidgetProps } from '../../Widget';
import {
  ResponsiveContainer,
  PieChart as PieChartRecharts,
  Pie,
  Legend,
  Curve,
} from 'recharts';
import { dataLimiter, DEFAULT_COLORS, OptionType } from '../../utils';

interface PieChartData {
  name: string;
  value: number;
  color?: string;
  label?: string;
}

export interface PieChartProps extends WidgetProps {
  data: PieChartData[];
  title?: string;
  titleScale?: number;
  showLegend?: boolean;
  titleColor?: string;
  titleBackgroundColor?: string;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}

const PieChart: React.FunctionComponent<PieChartProps> = ({
  rss,
  data,
  title,
  titleScale,
  scale = 1,
  margin,
  padding,
  border,
  showLegend,
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  titleAlign,
}) => {
  //data = dataLimiter(data);
  data = data.map((v, i) => ({
    value: v.value,
    name: v.label || v.name,
    fill: v.color || DEFAULT_COLORS[i % DEFAULT_COLORS.length],
  }));
  const radius = 64 * scale;
  return (
    <WidgetContainer
      rss={rss}
      title={title}
      titleScale={titleScale}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      contentContainerStyle={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      titleAlign={titleAlign}
    >
      <ResponsiveContainer>
        <PieChartRecharts>
          <Pie
            outerRadius={`${radius}%`}
            dataKey="value"
            nameKey="name"
            isAnimationActive={false}
            data={data}
            label={
              showLegend
                ? undefined
                : p => renderCustomPieLabel({ ...p, scale })
            }
            labelLine={
              showLegend ? false : p => renderCustomLabelLine({ ...p, scale })
            }
          />
          {showLegend && <Legend verticalAlign="bottom" height={36} />}
        </PieChartRecharts>
      </ResponsiveContainer>
    </WidgetContainer>
  );
};

export default PieChart;

const renderCustomPieLabel = ({ x, y, cx, cy, payload, scale }: any) => {
  const scaledLineLength = LINE_LENGTH * scale;
  const theta = Math.atan2(y - cy, x - cx);
  const distance = Math.sqrt(Math.pow(y - cy, 2) + Math.pow(x - cx, 2));
  x =
    cx + (distance - ORIGINAL_LINE_LENGTH + scaledLineLength) * Math.cos(theta);
  y =
    cy + (distance - ORIGINAL_LINE_LENGTH + scaledLineLength) * Math.sin(theta);
  return (
    <text
      x={x + (x > cx ? +1 : -1) * 3}
      y={y}
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
      fill={payload.fill}
      fontSize={14 * scale}
    >
      {payload.name}
    </text>
  );
};

const ORIGINAL_LINE_LENGTH = 20;
const LINE_LENGTH = 15;

const renderCustomLabelLine = (props: any) => {
  const scaledLineLength = LINE_LENGTH * props.scale;
  let x1 = props.points[0].x,
    x2 = props.points[1].x,
    y1 = props.points[0].y,
    y2 = props.points[1].y;
  const theta = Math.atan2(y2 - y1, x2 - x1);
  x2 = x1 + scaledLineLength * Math.cos(theta);
  y2 = y1 + scaledLineLength * Math.sin(theta);
  const points = [
    { x: x1, y: y1 },
    { x: x2, y: y2 },
  ];
  return (
    <Curve
      {...props}
      type="linear"
      className="recharts-pie-label-line"
      points={points}
    />
  );
};
