import React from 'react';
import { View, Text, StyleSheet } from '@react-pdf/renderer';
import {
  TableTecnoProps,
  LABEL_FONT_SIZE,
  DATA_FONT_SIZE,
  getFooterData,
} from './Table';
import {
  border,
  dataLimiter,
  getDateTimeFormat,
  getMomentTF,
  shortValue,
} from '../../utils';
import WidgetContainerPDF from '../../components/WidgetContainerPDF/WidgetContainerPDF';

const CELL_PADDING = 5;
const DAY_MILLI = 90000000;

export default function TablePDF({
  data,
  scale = 1,
  title,
  titleScale,
  border: borderData,
  margin,
  padding,
  decimalLimit,
  footerRows = { sums: true, avgs: true, mins: true, maxs: true },
  globalParameters = {},
  dateTimeFormat = 'DATETIME',
  titleColor,
  titleBackgroundColor,
  backgroundColor,
  hideDate,
  hideHeader,
  titleAlign,
}: TableTecnoProps) {
  const { timezone } = globalParameters;
  //data.data = dataLimiter(data.data);
  let isDayInterval = false;
  try {
    const firstDay = data.data[0].timestamp;
    const lastDay = data.data[data.data.length - 1].timestamp;
    isDayInterval = Math.abs(lastDay - firstDay) < DAY_MILLI;
  } catch (error) {}

  const dateFormat = getDateTimeFormat(dateTimeFormat);
  const cellWidth = 100 / data?.headers?.length || 100;

  const footerData = getFooterData(data, footerRows);

  return (
    <WidgetContainerPDF
      scale={scale}
      title={title}
      titleScale={titleScale}
      border={borderData}
      margin={margin}
      padding={padding}
      titleColor={titleColor}
      titleBackgroundColor={titleBackgroundColor}
      backgroundColor={backgroundColor}
      titleAlign={titleAlign}
    >
      <View style={styles.container} wrap={true}>
        {!hideHeader && (
          <View style={styles.thr} wrap={false} fixed>
            {!hideDate && <View style={[styles.tld]} />}
            {data.headers.map((h, i) => (
              <View style={[styles.td]} key={i}>
                <Text
                  style={[styles.tl, { fontSize: LABEL_FONT_SIZE * scale }]}
                >
                  {h.label || h.id}
                </Text>
              </View>
            ))}
          </View>
        )}
        {data.data.map((batch, i) => (
          <View style={i != 0 ? styles.tr : styles.trs} wrap={false} key={i}>
            {!hideDate && (
              <View style={[styles.tld]}>
                <Text
                  style={[
                    styles.tl,
                    styles.timestamp,
                    { fontSize: LABEL_FONT_SIZE * scale },
                  ]}
                >
                  {getMomentTF(batch.timestamp, timezone).format(dateFormat)}
                </Text>
              </View>
            )}
            {batch.values.map(rawValue => {
              const value = shortValue(rawValue, decimalLimit);
              return (
                <View style={[styles.td]}>
                  <Text
                    style={[styles.tt, { fontSize: DATA_FONT_SIZE * scale }]}
                  >
                    {value}
                  </Text>
                </View>
              );
            })}
          </View>
        ))}
        {footerData.map((row, i) => (
          <TableRow
            rowSeparator={i === 0}
            label={row.label}
            values={row.data}
            cellWidth={cellWidth}
            scale={scale}
            decimalLimit={decimalLimit}
            hideDate={hideDate}
          />
        ))}
      </View>
    </WidgetContainerPDF>
  );
}

const TableRow = ({
  rowSeparator = false,
  label,
  values,
  decimalLimit,
  scale = 1,
  hideDate,
}: {
  rowSeparator?: boolean;
  label: string;
  values: number[];
  cellWidth: number;
  scale: number;
  decimalLimit?: false | number;
  hideDate?: boolean;
}) => (
  <View style={rowSeparator ? styles.trs : styles.tr} wrap={false}>
    {!hideDate && (
      <View style={[styles.tld]}>
        <Text
          style={[
            styles.tl,
            styles.timestamp,
            { fontSize: LABEL_FONT_SIZE * scale },
          ]}
        >
          {label}
        </Text>
      </View>
    )}
    {values.map(rawValue => {
      const value = shortValue(rawValue, decimalLimit);
      return (
        <View style={[styles.td]}>
          <Text style={[styles.tt, { fontSize: DATA_FONT_SIZE * scale }]}>
            {value}
          </Text>
        </View>
      );
    })}
  </View>
);

const styles = StyleSheet.create({
  container: {
    //@ts-ignore
    display: 'table',
    width: 'auto',
  },
  tr: {
    flexDirection: 'row',
    ...border('#eaeaea', [1]),
  },
  trs: {
    flexDirection: 'row',
    ...border('#1de9b6', [2]),
  },
  thr: {
    flexDirection: 'row',
    ...border('#eaeaea', [1]),
  },
  td: {
    justifyContent: 'center',
    flex: 1,
    padding: CELL_PADDING,
  },
  tld: {
    width: '60px',
    ...border('#eaeaea', [0, 1]),
  },
  timestamp: {
    textAlign: 'left',
  },
  tl: {
    fontSize: 12,
    textOverflow: 'ellipsis',
    maxLines: 3,
    maxWidth: '100%',
    color: '#888888',
    padding: 5,
    textAlign: 'center',
  },
  tt: {
    fontSize: 12,
    textOverflow: 'ellipsis',
    maxLines: 3,
    maxWidth: '100%',
    textAlign: 'center',
  },
});
