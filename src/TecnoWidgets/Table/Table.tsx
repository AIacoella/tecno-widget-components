import React from 'react';
import { WidgetContainer, WidgetProps } from '../../Widget';
import _, { size } from 'lodash';
import styles from './Table.module.css';
import {
  dataLimiter,
  getDateTimeFormat,
  getMomentTF,
  OptionType,
  shortValue,
} from '../../utils';
import classnames from 'classnames';
import { FixedSizeList as List } from 'react-window';
import { SizeMe } from 'react-sizeme';

export const LABEL_FONT_SIZE = 12;
export const DATA_FONT_SIZE = 12;
export const LABEL_WIDTH = 80;

interface HeaderInterface {
  id: string;
  label?: string;
}

type FooterRowType = {
  sums: boolean;
  avgs: boolean;
  mins: boolean;
  maxs: boolean;
};

interface TableDataInterface {
  headers: HeaderInterface[];
  data: { timestamp: number; values: number[] }[];

  sums?: number[];
  avgs?: number[];
  mins?: number[];
  maxs?: number[];
}

export interface TableTecnoProps extends WidgetProps {
  data: TableDataInterface;
  footerRows?: FooterRowType;
  title?: string;
  titleScale?: number;
  titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
  decimalLimit?: false | number;
  dateTimeFormat?: 'DATE' | 'TIME' | 'DATETIME';
  dataLimit?: number;
  titleColor?: string;
  titleBackgroundColor?: string;
  hideDate?: boolean;
  hideHeader?: boolean;
}

const DAY_MILLI = 90000000;
const ROW_HEIGHT = 36;

const TableTecno: React.FunctionComponent<TableTecnoProps> = ({
  rss,
  data,
  title,
  titleScale,
  scale = 1,
  margin,
  padding,
  border,
  footerRows = { sums: true, avgs: true, mins: true, maxs: true },
  decimalLimit,
  globalParameters = {},
  dateTimeFormat = 'DATETIME',
  dataLimit,
  titleBackgroundColor,
  titleColor,
  backgroundColor,
  hideDate,
  hideHeader,
  titleAlign,
}) => {
  const { timezone } = globalParameters;
  if (_.isEmpty(data)) return null;
  if (dataLimit) data.data = dataLimiter(data.data, dataLimit);
  let isDayInterval = false;
  try {
    const firstDay = data.data[0].timestamp;
    const lastDay = data.data[data.data.length - 1].timestamp;
    isDayInterval = Math.abs(lastDay - firstDay) < DAY_MILLI;
  } catch (error) {}

  const dateFormat = getDateTimeFormat(dateTimeFormat);
  const footerData = getFooterData(data, footerRows);

  const nValues = data?.data?.length > 0 ? data.data[0].values.length : 0;
  const rowStyle = {
    height: ROW_HEIGHT,
    gridTemplateColumns:
      (!hideDate ? '100px ' : '') +
      Array(nValues)
        .fill(0)
        .reduce(str => str + '1fr ', ''),
  };

  if (rss)
    return (
      <WidgetContainer
        rss={rss}
        title={title}
        titleScale={titleScale}
        scale={scale}
        margin={margin}
        padding={padding}
        border={border}
        titleBackgroundColor={titleBackgroundColor}
        titleColor={titleColor}
        backgroundColor={backgroundColor}
        titleAlign={titleAlign}
      >
        <SSTable
          data={data}
          footerData={footerData}
          rowStyle={rowStyle}
          timezone={timezone}
          dateFormat={dateFormat}
          decimalLimit={decimalLimit}
          scale={scale}
          hideDate={hideDate}
          hideHeader={hideHeader}
        />
      </WidgetContainer>
    );

  return (
    <WidgetContainer
      rss={rss}
      title={title}
      scale={scale}
      margin={margin}
      padding={padding}
      border={border}
      titleBackgroundColor={titleBackgroundColor}
      titleColor={titleColor}
      backgroundColor={backgroundColor}
      titleAlign={titleAlign}
    >
      <SizeMe monitorHeight={true} monitorWidth={false}>
        {({ size }) => (
          <div className={styles.table}>
            {!hideHeader && (
              <div className={styles.heading} style={rowStyle}>
                {!hideDate && (
                  <div
                    className={classnames(styles.cell, styles.label)}
                    style={{ width: LABEL_WIDTH * scale }}
                  />
                )}
                {data.headers.map((header, i) => (
                  <div
                    key={i}
                    className={styles.cell}
                    style={{ fontSize: LABEL_FONT_SIZE * scale }}
                  >
                    {header.label || header.id}
                  </div>
                ))}
              </div>
            )}
            <List
              height={size.height || 0}
              width={'100%'}
              itemCount={data.data.length + footerData.length}
              itemSize={ROW_HEIGHT}
            >
              {({ index, style }) =>
                index < data.data.length ? (
                  <TableRow
                    key={index}
                    label={getMomentTF(
                      data.data[index].timestamp,
                      timezone
                    ).format(dateFormat)}
                    data={data.data[index].values}
                    rowStyle={{ ...rowStyle, ...style }}
                    scale={scale}
                    decimalLimit={decimalLimit}
                    hideLabel={hideDate}
                  />
                ) : (
                  <TableRow
                    key={index}
                    label={footerData[index - data.data.length].label}
                    data={footerData[index - data.data.length].data}
                    rowStyle={{ ...rowStyle, ...style }}
                    scale={scale}
                    decimalLimit={decimalLimit}
                    separator={index === data.data.length}
                    hideLabel={hideDate}
                  />
                )
              }
            </List>
          </div>
        )}
      </SizeMe>
    </WidgetContainer>
  );
};

export default TableTecno;

const TableRow = ({
  data,
  label,
  separator,
  scale,
  decimalLimit,
  rowStyle,
  hideLabel,
}: {
  data: number[];
  label: string;
  separator?: boolean;
  scale: number;
  decimalLimit?: false | number;
  rowStyle: React.CSSProperties;
  hideLabel?: boolean;
}) => (
  <div
    className={classnames(
      styles.row,
      separator ? styles.rowSeparator : undefined
    )}
    style={rowStyle}
  >
    {!hideLabel && (
      <div
        className={classnames(styles.cell, styles.label)}
        style={{
          fontSize: LABEL_FONT_SIZE * scale,
          width: LABEL_WIDTH * scale,
        }}
      >
        {label}
      </div>
    )}
    {data.map((v, index) => (
      <div
        className={styles.cell}
        key={index}
        style={{ fontSize: DATA_FONT_SIZE * scale }}
      >
        {shortValue(v, decimalLimit)}
      </div>
    ))}
  </div>
);

interface SSTableProps {
  data: TableDataInterface;
  footerData: { label: string; data: number[] }[];
  rowStyle: React.CSSProperties;
  timezone?: string;
  dateFormat: 'DD/MM' | 'HH:mm' | 'DD/MM HH:mm';
  decimalLimit?: false | number;
  scale: number;
  hideDate?: boolean;
  hideHeader?: boolean;
}

const SSTable = ({
  rowStyle,
  data,
  footerData,
  timezone,
  dateFormat,
  decimalLimit,
  scale,
  hideDate,
  hideHeader,
}: SSTableProps) => (
  <div className={classnames(styles.table, 'RE-scrollable-container')}>
    {!hideHeader && (
      <div className={styles.heading} style={rowStyle}>
        {!hideDate && (
          <div
            className={classnames(styles.cell, styles.label)}
            style={{ width: LABEL_WIDTH * scale }}
          />
        )}
        {data.headers.map((header, i) => (
          <div
            key={i}
            className={styles.cell}
            style={{ fontSize: LABEL_FONT_SIZE * scale }}
          >
            {header.label || header.id}
          </div>
        ))}
      </div>
    )}
    <div>
      {data.data.map(({ timestamp, values }, index) => (
        <TableRow
          key={index}
          label={getMomentTF(timestamp, timezone).format(dateFormat)}
          data={values}
          rowStyle={rowStyle}
          scale={scale}
          decimalLimit={decimalLimit}
          hideLabel={hideDate}
        />
      ))}
      {footerData.map(({ label, data }, index) => (
        <TableRow
          key={index}
          label={label}
          data={data}
          rowStyle={rowStyle}
          scale={scale}
          decimalLimit={decimalLimit}
          separator={index === 0}
          hideLabel={hideDate}
        />
      ))}
    </div>
  </div>
);

export const getFooterData = (
  {
    sums,
    avgs,
    mins,
    maxs,
  }: {
    sums?: number[];
    avgs?: number[];
    mins?: number[];
    maxs?: number[];
  },
  footerRows: FooterRowType
) => {
  const footerData = [];
  if (sums && footerRows.sums) footerData.push({ label: 'Sum', data: sums });
  if (avgs && footerRows.avgs) footerData.push({ label: 'Avg', data: avgs });
  if (mins && footerRows.mins) footerData.push({ label: 'Min', data: mins });
  if (maxs && footerRows.maxs) footerData.push({ label: 'Max', data: maxs });
  return footerData;
};
