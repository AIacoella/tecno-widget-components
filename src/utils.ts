import _ from 'lodash';
import moment, { Moment } from 'moment-timezone';
import { FourBorderType, FourIntegerType } from './Widget';

const BORDER_SIDES = ['borderTop', 'borderRight', 'borderBottom', 'borderLeft'];

export const dataLimiter = (data: any[], limit = 200) => {
  const clonedData = _.cloneDeep(data);
  if (clonedData.length > limit) {
    const rate = clonedData.length / (clonedData.length - limit);
    let i = 0;
    while (i < clonedData.length) {
      clonedData.splice(Math.ceil(i), 1);
      i += rate - 1;
    }
    return clonedData;
  } else return clonedData;
};

const SAFETY_LIMIT = 1000;
export const safetyLimit = (data: any[], numSources: number = 1) => {
  if (data.length * numSources > SAFETY_LIMIT)
    return dataLimiter(data, Math.trunc(SAFETY_LIMIT / numSources));
  return data;
};

export const buildStyleString = (data: FourIntegerType, suffix = '') =>
  data.reduce(
    (state, value, index) =>
      state + '' + value + '' + suffix + (index !== data.length - 1 ? ' ' : ''),
    ''
  );

const BORDER_NULL_STYLE = { width: 'none', color: 'none' };

export const buildBorderStyle = (value: FourBorderType) => {
  if (typeof value !== 'object' || _.isEmpty(value)) return BORDER_NULL_STYLE;
  const widthStr = value.reduce(
    (state, v, i) =>
      state +
      (v.borderWidth ? v.borderWidth + 'px' : '0') +
      (i !== value.length - 1 ? ' ' : ''),
    ''
  );
  const colorStr = value.reduce(
    (state, v, i) =>
      state +
      (v.borderColor
        ? '#' + (v.borderColor + '').replace('#', '')
        : '#111111') +
      (i !== value.length - 1 ? ' ' : ''),
    ''
  );
  return { width: widthStr, color: colorStr };
};

export const border = (
  color: string,
  sides: number[] = [1, 1, 1, 1],
  style: 'solid' | 'dashed' = 'solid'
) => {
  const b: any = {
    borderColor: color,
    borderStyle: style,
  };
  sides.forEach((f, i) => (b[BORDER_SIDES[i] + 'Width'] = f));
  return b;
};

export const advancedBorder = (value: FourBorderType) => {
  const style: any = {
    borderStyle: 'solid',
  };
  value.forEach(({ borderWidth, borderColor }, i) => {
    style[BORDER_SIDES[i] + 'Width'] = borderWidth ? borderWidth : 0;
    style[BORDER_SIDES[i] + 'Color'] = borderColor
      ? '#' + borderColor.replace('#', '')
      : '#111111';
  });
  return style;
};

export const getMomentTF = (inp: any, timezone?: string) => {
  if (!timezone) timezone = moment.tz.guess();
  return moment.tz(inp, timezone);
};

export const nowDateTF = (timezone?: string) => {
  if (!timezone) timezone = moment.tz.guess();
  const currentTime = new Date();
  const convertTime = moment(currentTime)
    .tz(timezone)
    .format('YYYY-MM-DD HH:mm:ss');
  return new Date(convertTime);
};

type ValueUomType = {
  value: number;
  uom: string;
};

export interface TimeframeInterface {
  name: string;

  from?: string;
  to?: string;

  previousOffset?: ValueUomType;
  interval?: ValueUomType;
  offsetFrom?: ValueUomType;
  startTime?: string;
  endTime?: string;
}

export interface GlobalParametersInterface {
  timezone?: string;
  metrics?: string[];
  company?: string;
  timeframe?: TimeframeInterface;
}

export const limitDecimals = (
  value: number | string,
  decimalLimiter?: false | number
) => {
  value = Number(value);
  if (typeof decimalLimiter === 'number') return +value.toFixed(decimalLimiter);
  else return value;
};

export function numberWithCommas(x: number) {
  return x.toLocaleString('it');
}

export const shortValue = (
  value?: number | string,
  decimalLimit?: false | number
) => {
  value = Number(value);
  value = +value.toFixed(6);
  return limitDecimals(value, decimalLimit);
};

export const hex = (value?: string) =>
  value ? `#${value.replace('#', '')}` : undefined;

export const DEFAULT_COLORS = [
  '#3f4d67',
  '#673f4d',
  '#3f6759',
  '#67593f',
  '#da8086',
  '#f5b6ad',
  '#fddac2',
];

export const getDateTimeFormat = (
  dateTimeFormat: 'DATE' | 'TIME' | 'DATETIME'
) => {
  if (dateTimeFormat === 'DATE') return 'DD/MM';
  else if (dateTimeFormat === 'TIME') return 'HH:mm';
  else return 'DD/MM HH:mm';
};

export type OptionType<V = string> = { value: V; label: string };

export const DEFAULT_ALIGN: OptionType<'LEFT' | 'CENTER' | 'RIGHT'> = {
  value: 'CENTER',
  label: 'Center',
};

export const DEFAULT_LEFT_ALIGN: OptionType<'LEFT' | 'CENTER' | 'RIGHT'> = {
  value: 'LEFT',
  label: 'Left',
};

export const getTextAlign = (
  alignText: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>
): 'left' | 'right' | 'center' => {
  if (alignText.value === 'LEFT') return 'left';
  if (alignText.value === 'RIGHT') return 'right';
  return 'center';
};

export const getFlexOrientation = (
  alignText: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>
) => {
  if (alignText.value === 'LEFT') return 'flex-start';
  if (alignText.value === 'RIGHT') return 'flex-end';
  return 'center';
};

export const interpolator = (
  data: { [key: string]: number; timestamp: number }[],
  ids: string[]
) => {
  const settings: { [key: string]: any } = {};
  ids.forEach(
    id =>
      (settings[id] = {
        lastY: 0,
        width: 0,
        hasMissingPoint: false,
        hasFoundFirstPoint: false,
      })
  );

  for (let i = 0; i < data.length; i++) {
    for (const id of ids) {
      if (data[i][id] != null) {
        if (settings[id].hasMissingPoint && settings[id].width !== 0) {
          settings[id].width += data[i].timestamp - data[i - 1].timestamp;
          let j = i - 1;
          const ratio = (data[i][id] - settings[id].lastY) / settings[id].width;

          while (data[j][id] == null) {
            const dw = data[j].timestamp - data[j + 1].timestamp;
            const newY = data[j + 1][id] + dw * ratio;
            data[j][id] = newY;
            j--;
          }
        }
        settings[id].lastY = data[i][id];
        settings[id].width = 0;
        settings[id].hasMissingPoint = false;
        settings[id].hasFoundFirstPoint = true;
      } else {
        if (settings[id].hasFoundFirstPoint) {
          settings[id].width += data[i].timestamp - data[i - 1].timestamp;
          settings[id].hasMissingPoint = true;
        }
      }
    }
  }
  return data;
};

export const getTimeframe = (timeframe: any, timezone?: string) => {
  if (!timezone) timezone = moment.tz.guess();
  //console.log(timeframe);
  timeframe = vivifyTimeframe(timeframe, timezone);
  let fromTS, toTS;

  if (timeframe.interval != null) {
    let from = moment.tz(undefined, timezone);
    if (timeframe.previousOffset)
      from = processPreviousOffset(from, timeframe.previousOffset);
    let to = from
      .clone()
      .add(timeframe.interval.value, toMomentUnit(timeframe.interval.uom));
    if (isUomToSub(timeframe.interval.uom)) to = to.subtract(1, 'second');
    if (timeframe.offsetFrom) from = processOffset(from, timeframe.offsetFrom);
    if (timeframe.offsetTo) to = processOffset(to, timeframe.offsetTo);
    if (timeframe.startTime != null)
      from = processTimeOverride(from, timeframe.startTime);
    if (timeframe.endTime != null)
      to = processTimeOverride(to, timeframe.endTime);

    fromTS = _.toNumber(from.format('x'));
    toTS = _.toNumber(to.format('x'));
  } else {
    //Absolute
    const { from, to } = timeframe;
    fromTS = from ? _.toNumber(from.format('x')) : undefined;
    toTS = _.toNumber(
      to ? to.format('x') : moment.tz(undefined, timezone).format('x')
    );
  }
  return { from: fromTS, to: toTS };
};

export const vivifyTimeframe = (timeframe: any, timezone: string) => {
  if (!timeframe) return undefined;
  if (timeframe.from && typeof timeframe.from === 'string')
    timeframe.from = moment.tz(timeframe.from, timezone);
  if (timeframe.to && typeof timeframe.to === 'string')
    timeframe.to = moment.tz(timeframe.to, timezone);
  return timeframe;
};

export const processDynamicOffset = (
  date: Moment,
  dynamicOffset: number,
  uom: string
) => {
  return date
    .startOf(toMomentUnit(uom))
    .subtract(dynamicOffset, toMomentUnit(uom));
};

const processPreviousOffset = (date: Moment, { value, uom }: ValueUomType) => {
  return date.startOf(toMomentUnit(uom)).subtract(value, toMomentUnit(uom));
};

const processOffset = (date: Moment, { value, uom }: ValueUomType) => {
  return date.add(value, toMomentUnit(uom));
};

const processTimeOverride = (date: Moment, time: string) => {
  if (isValidTime(time)) {
    const hours = Number(time.substring(0, time.indexOf(':')));
    const minutes = Number(time.substring(time.indexOf(':') + 1));
    date.set('hours', hours);
    date.set('minutes', minutes);
  } else if (time === 'NOW') {
    const now = moment();
    date.set('hours', now.get('hours'));
    date.set('minutes', now.get('minutes'));
  }
  return date;
};

const isValidTime = (value: any) =>
  typeof value === 'string' &&
  /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(value);

const toMomentUnit = (tecnoUnit: string) => {
  //@ts-ignore
  const unit = TECNO_UOMS_TO_MOMENT[tecnoUnit];
  return unit || 'hours';
};

const TECNO_UOMS_TO_MOMENT = {
  HOURS: 'hours',
  DAYS: 'days',
  WEEKS: 'isoWeek',
  MONTHS: 'months',
  YEARS: 'years',
  MINUTES: 'minutes',
};

const TIMEFRAME_UOMS = {
  HOURS: { label: 'Hours', value: 'HOURS' },
  DAYS: { label: 'Days', value: 'DAYS' },
  WEEKS: { label: 'Weeks', value: 'WEEKS' },
  MONTHS: { label: 'Months', value: 'MONTHS' },
  YEARS: { label: 'Years', value: 'YEARS' },
  MINUTES: { label: 'Minutes', value: 'MINUTES' },
};

const isUomToSub = (uom: string) =>
  uom === TIMEFRAME_UOMS.DAYS.value ||
  uom === TIMEFRAME_UOMS.WEEKS.value ||
  uom === TIMEFRAME_UOMS.MONTHS.value ||
  uom === TIMEFRAME_UOMS.YEARS.value;
