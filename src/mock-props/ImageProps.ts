import { ImageProps } from '../TecnoWidgets/Image/Image';

const props: ImageProps = {
  scale: 1,
  url: 'https://www.w3schools.com/howto/img_woods_wide.jpg',
};

export default props;
