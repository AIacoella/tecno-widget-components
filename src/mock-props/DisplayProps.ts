import { DisplayProps } from '../TecnoWidgets/Display/Display';

const props: DisplayProps = {
  data: '3231.234',
  decimalLimit: 2,
  prefix: '$',
  scale: 1,
  label: 'Some label here',
};

export default props;
