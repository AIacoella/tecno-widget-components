import { PieChartProps } from 'TecnoWidgets/PieChart/PieChart';

const props: PieChartProps = {
  title: 'BarChart',
  data: [
    {
      name: 'Group 1',
      label: 'Test label',
      value: 194912.26254607315,
    },
    {
      name: 'Group 2',
      value: 224806.3679043961,
    },
    {
      name: 'Group 3',
      value: 227946.81575246132,
    },
    {
      name: 'Group 4',
      color: '#008812',
      value: 230621.58663296828,
    },
    {
      name: 'Group 5',
      value: 231712.44167962676,
    },
    {
      name: 'Group 6',
      value: 230919.32614555256,
    },
  ],
  scale: 1,
  margin: [0, 0, 0, 200],
};

export default props;
