import { ClockProps } from '../TecnoWidgets/Clock/Clock';

const props: ClockProps = {
  scale: 1,
};

export default props;
