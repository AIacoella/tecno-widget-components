import { ParagraphProps } from '../TecnoWidgets/Paragraph/Paragraph';

const props: ParagraphProps = {
  scale: 1,
  content:
    "# H1\n## H2\n### H3\nEsempio ${NOW_DATETIME}\nThis is an normal paragraph. **This section is in bold** *and this in italic*.\n[Link](http://a.com)\n> Blockquote\n> Continuing on the blockquote\n* List\n* List\n* List\n1. One\n2. Two\n3. Three\n---\n`Inline code` with backticks\n```\n# code block\nprint '3 backticks or'\nprint 'indent 4 spaces'\n```",
};

/* 
"# Header 1\n## Header 2\nP1\nP2\n\nList\n- listItem1\n- listItem2\n\n2. One\n4. Two\n\nTest [Link](www.google.com) Test\n\n> BlockQuote\n> This is **bold**\n> This is *italic*\n> This is **bold*italic*bold**\n\n---\n\n`Inline code` with backticks\n\n```\n# code block\nprint '3 backticks or'\nprint 'indent 4 spaces'\n```",
*/

export default props;
