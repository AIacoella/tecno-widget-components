import { MultiChartProps } from '../TecnoWidgets/Timeseries/MultiChart/MultiChart';

const props: MultiChartProps = {
  title: 'TimeSeries',
  xAxisLabel: 'X-UOM',
  yAxisLabel: 'Y-UOM',
  data: {
    sources: [
      { id: 'metric1', chartType: 'LINE', axis: 0 },
      { id: 'metric2', chartType: 'LINE', axis: 0 },
    ],
    data: [
      {
        timestamp: 1594185600000,
        metric1: 194912.26254607315,
        metric2: 194912.26254607315 / 2,
      },
      {
        timestamp: 1594188000000,
        metric1: 224806.3679043961,
      },
      {
        timestamp: 1594190400000,
        metric1: 227946.81575246132,
        metric2: 227946.81575246132 * 2,
      },
      {
        timestamp: 1594192800000,
      },
      {
        timestamp: 1594195200000,
      },
      {
        timestamp: 1594197600000,
        metric1: 230919.32614555256,
        metric2: 230919.32614555256,
      },
    ],
  },
  showLegend: true,
  scale: 1,
};

export default props;
