import TimeSeriesTecno from './TimeSeriesTecnoProps';
import ParagraphTecno from './ParagraphProps';
import TableTecno from './TableTecnoProps';
import HeaderTecno from './HeaderProps';
import Display from './DisplayProps';
import BarChart from './BarChartProps';
import Clock from './ClockProps';
import Image from './ImageProps';
import PieChart from './PieChartProps';
import MultiChart from './MultiChartProps';

export default {
  TimeSeriesTecno,
  ParagraphTecno,
  HeaderTecno,
  TableTecno,
  Display,
  BarChart,
  Clock,
  Image,
  PieChart,
  MultiChart,
};
