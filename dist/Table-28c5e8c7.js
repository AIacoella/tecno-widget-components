import React, { useState, useEffect, isValidElement, cloneElement } from 'react';
import _ from 'lodash-es';
import moment from 'moment-timezone';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import classnames from 'classnames';
import sanitizeHtml from 'sanitize-html';
import { FixedSizeList } from 'react-window';
import { SizeMe } from 'react-sizeme';

var styles = {"header":"Header-module_header__2gLzh","container":"Header-module_container__3_Tvo"};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelperLoose(o, allowArrayLike) {
  var it;

  if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;
      return function () {
        if (i >= o.length) return {
          done: true
        };
        return {
          done: false,
          value: o[i++]
        };
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  it = o[Symbol.iterator]();
  return it.next.bind(it);
}

var styles$1 = {"widgetContainer":"styles-module_widgetContainer__CeqVs","widgetContentContainer":"styles-module_widgetContentContainer__1MS2F","recharts-cartesian-axis-tick-value":"styles-module_recharts-cartesian-axis-tick-value__1KWIe","widgetContainerContainer":"styles-module_widgetContainerContainer__dhQ4i"};

var BORDER_SIDES = ['borderTop', 'borderRight', 'borderBottom', 'borderLeft'];
var dataLimiter = function dataLimiter(data, limit) {
  if (limit === void 0) {
    limit = 200;
  }

  var clonedData = _.cloneDeep(data);

  if (clonedData.length > limit) {
    var rate = clonedData.length / (clonedData.length - limit);
    var i = 0;

    while (i < clonedData.length) {
      clonedData.splice(Math.ceil(i), 1);
      i += rate - 1;
    }

    return clonedData;
  } else return clonedData;
};
var SAFETY_LIMIT = 1000;
var safetyLimit = function safetyLimit(data, numSources) {
  if (numSources === void 0) {
    numSources = 1;
  }

  if (data.length * numSources > SAFETY_LIMIT) return dataLimiter(data, Math.trunc(SAFETY_LIMIT / numSources));
  return data;
};
var buildStyleString = function buildStyleString(data, suffix) {
  if (suffix === void 0) {
    suffix = '';
  }

  return data.reduce(function (state, value, index) {
    return state + '' + value + '' + suffix + (index !== data.length - 1 ? ' ' : '');
  }, '');
};
var BORDER_NULL_STYLE = {
  width: 'none',
  color: 'none'
};
var buildBorderStyle = function buildBorderStyle(value) {
  if (typeof value !== 'object' || _.isEmpty(value)) return BORDER_NULL_STYLE;
  var widthStr = value.reduce(function (state, v, i) {
    return state + (v.borderWidth ? v.borderWidth + 'px' : '0') + (i !== value.length - 1 ? ' ' : '');
  }, '');
  var colorStr = value.reduce(function (state, v, i) {
    return state + (v.borderColor ? '#' + (v.borderColor + '').replace('#', '') : '#111111') + (i !== value.length - 1 ? ' ' : '');
  }, '');
  return {
    width: widthStr,
    color: colorStr
  };
};
var border = function border(color, sides, style) {
  if (sides === void 0) {
    sides = [1, 1, 1, 1];
  }

  if (style === void 0) {
    style = 'solid';
  }

  var b = {
    borderColor: color,
    borderStyle: style
  };
  sides.forEach(function (f, i) {
    return b[BORDER_SIDES[i] + 'Width'] = f;
  });
  return b;
};
var advancedBorder = function advancedBorder(value) {
  var style = {
    borderStyle: 'solid'
  };
  value.forEach(function (_ref, i) {
    var borderWidth = _ref.borderWidth,
        borderColor = _ref.borderColor;
    style[BORDER_SIDES[i] + 'Width'] = borderWidth ? borderWidth : 0;
    style[BORDER_SIDES[i] + 'Color'] = borderColor ? '#' + borderColor.replace('#', '') : '#111111';
  });
  return style;
};
var getMomentTF = function getMomentTF(inp, timezone) {
  if (!timezone) timezone = moment.tz.guess();
  return moment.tz(inp, timezone);
};
var nowDateTF = function nowDateTF(timezone) {
  if (!timezone) timezone = moment.tz.guess();
  var currentTime = new Date();
  var convertTime = moment(currentTime).tz(timezone).format('YYYY-MM-DD HH:mm:ss');
  return new Date(convertTime);
};
var limitDecimals = function limitDecimals(value, decimalLimiter) {
  value = Number(value);
  if (typeof decimalLimiter === 'number') return +value.toFixed(decimalLimiter);else return value;
};
function numberWithCommas(x) {
  return x.toLocaleString('it');
}
var shortValue = function shortValue(value, decimalLimit) {
  value = Number(value);
  value = +value.toFixed(6);
  return limitDecimals(value, decimalLimit);
};
var hex = function hex(value) {
  return value ? "#" + value.replace('#', '') : undefined;
};
var DEFAULT_COLORS = ['#3f4d67', '#673f4d', '#3f6759', '#67593f', '#da8086', '#f5b6ad', '#fddac2'];
var getDateTimeFormat = function getDateTimeFormat(dateTimeFormat) {
  if (dateTimeFormat === 'DATE') return 'DD/MM';else if (dateTimeFormat === 'TIME') return 'HH:mm';else return 'DD/MM HH:mm';
};
var DEFAULT_ALIGN = {
  value: 'CENTER',
  label: 'Center'
};
var DEFAULT_LEFT_ALIGN = {
  value: 'LEFT',
  label: 'Left'
};
var getTextAlign = function getTextAlign(alignText) {
  if (alignText.value === 'LEFT') return 'left';
  if (alignText.value === 'RIGHT') return 'right';
  return 'center';
};
var getFlexOrientation = function getFlexOrientation(alignText) {
  if (alignText.value === 'LEFT') return 'flex-start';
  if (alignText.value === 'RIGHT') return 'flex-end';
  return 'center';
};
var interpolator = function interpolator(data, ids) {
  var settings = {};
  ids.forEach(function (id) {
    return settings[id] = {
      lastY: 0,
      width: 0,
      hasMissingPoint: false,
      hasFoundFirstPoint: false
    };
  });

  for (var i = 0; i < data.length; i++) {
    for (var _iterator = _createForOfIteratorHelperLoose(ids), _step; !(_step = _iterator()).done;) {
      var id = _step.value;

      if (data[i][id] != null) {
        if (settings[id].hasMissingPoint && settings[id].width !== 0) {
          settings[id].width += data[i].timestamp - data[i - 1].timestamp;
          var j = i - 1;
          var ratio = (data[i][id] - settings[id].lastY) / settings[id].width;

          while (data[j][id] == null) {
            var dw = data[j].timestamp - data[j + 1].timestamp;
            var newY = data[j + 1][id] + dw * ratio;
            data[j][id] = newY;
            j--;
          }
        }

        settings[id].lastY = data[i][id];
        settings[id].width = 0;
        settings[id].hasMissingPoint = false;
        settings[id].hasFoundFirstPoint = true;
      } else {
        if (settings[id].hasFoundFirstPoint) {
          settings[id].width += data[i].timestamp - data[i - 1].timestamp;
          settings[id].hasMissingPoint = true;
        }
      }
    }
  }

  return data;
};
var getTimeframe = function getTimeframe(timeframe, timezone) {
  if (!timezone) timezone = moment.tz.guess(); //console.log(timeframe);

  timeframe = vivifyTimeframe(timeframe, timezone);
  var fromTS, toTS;

  if (timeframe.interval != null) {
    var from = moment.tz(undefined, timezone);
    if (timeframe.previousOffset) from = processPreviousOffset(from, timeframe.previousOffset);
    var to = from.clone().add(timeframe.interval.value, toMomentUnit(timeframe.interval.uom));
    if (isUomToSub(timeframe.interval.uom)) to = to.subtract(1, 'second');
    if (timeframe.offsetFrom) from = processOffset(from, timeframe.offsetFrom);
    if (timeframe.offsetTo) to = processOffset(to, timeframe.offsetTo);
    if (timeframe.startTime != null) from = processTimeOverride(from, timeframe.startTime);
    if (timeframe.endTime != null) to = processTimeOverride(to, timeframe.endTime);
    fromTS = _.toNumber(from.format('x'));
    toTS = _.toNumber(to.format('x'));
  } else {
    //Absolute
    var _timeframe = timeframe,
        _from = _timeframe.from,
        _to = _timeframe.to;
    fromTS = _from ? _.toNumber(_from.format('x')) : undefined;
    toTS = _.toNumber(_to ? _to.format('x') : moment.tz(undefined, timezone).format('x'));
  }

  return {
    from: fromTS,
    to: toTS
  };
};
var vivifyTimeframe = function vivifyTimeframe(timeframe, timezone) {
  if (!timeframe) return undefined;
  if (timeframe.from && typeof timeframe.from === 'string') timeframe.from = moment.tz(timeframe.from, timezone);
  if (timeframe.to && typeof timeframe.to === 'string') timeframe.to = moment.tz(timeframe.to, timezone);
  return timeframe;
};

var processPreviousOffset = function processPreviousOffset(date, _ref2) {
  var value = _ref2.value,
      uom = _ref2.uom;
  return date.startOf(toMomentUnit(uom)).subtract(value, toMomentUnit(uom));
};

var processOffset = function processOffset(date, _ref3) {
  var value = _ref3.value,
      uom = _ref3.uom;
  return date.add(value, toMomentUnit(uom));
};

var processTimeOverride = function processTimeOverride(date, time) {
  if (isValidTime(time)) {
    var hours = Number(time.substring(0, time.indexOf(':')));
    var minutes = Number(time.substring(time.indexOf(':') + 1));
    date.set('hours', hours);
    date.set('minutes', minutes);
  } else if (time === 'NOW') {
    var now = moment();
    date.set('hours', now.get('hours'));
    date.set('minutes', now.get('minutes'));
  }

  return date;
};

var isValidTime = function isValidTime(value) {
  return typeof value === 'string' && /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(value);
};

var toMomentUnit = function toMomentUnit(tecnoUnit) {
  //@ts-ignore
  var unit = TECNO_UOMS_TO_MOMENT[tecnoUnit];
  return unit || 'hours';
};

var TECNO_UOMS_TO_MOMENT = {
  HOURS: 'hours',
  DAYS: 'days',
  WEEKS: 'isoWeek',
  MONTHS: 'months',
  YEARS: 'years',
  MINUTES: 'minutes'
};
var TIMEFRAME_UOMS = {
  HOURS: {
    label: 'Hours',
    value: 'HOURS'
  },
  DAYS: {
    label: 'Days',
    value: 'DAYS'
  },
  WEEKS: {
    label: 'Weeks',
    value: 'WEEKS'
  },
  MONTHS: {
    label: 'Months',
    value: 'MONTHS'
  },
  YEARS: {
    label: 'Years',
    value: 'YEARS'
  },
  MINUTES: {
    label: 'Minutes',
    value: 'MINUTES'
  }
};

var isUomToSub = function isUomToSub(uom) {
  return uom === TIMEFRAME_UOMS.DAYS.value || uom === TIMEFRAME_UOMS.WEEKS.value || uom === TIMEFRAME_UOMS.MONTHS.value || uom === TIMEFRAME_UOMS.YEARS.value;
};

var styles$2 = {"container":"WidgetHeader-module_container__3rHxE","header":"WidgetHeader-module_header__2QEZd"};

var WIDGET_TITLE_SIZE = 24;

var WidgetHeader = function WidgetHeader(_ref) {
  var title = _ref.title,
      _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale,
      _ref$titleColor = _ref.titleColor,
      titleColor = _ref$titleColor === void 0 ? '#212224' : _ref$titleColor,
      titleBackgroundColor = _ref.titleBackgroundColor,
      _ref$titleAlign = _ref.titleAlign,
      titleAlign = _ref$titleAlign === void 0 ? DEFAULT_LEFT_ALIGN : _ref$titleAlign;
  return React.createElement("div", {
    className: styles$2.container,
    style: {
      backgroundColor: titleBackgroundColor
    }
  }, React.createElement("h3", {
    className: styles$2.header,
    style: {
      fontSize: scale * WIDGET_TITLE_SIZE,
      color: titleColor,
      textAlign: getTextAlign(titleAlign)
    }
  }, title));
};

var styles$3 = {"tooltipContainer":"WidgetWarning-module_tooltipContainer__6B1kf","popoverBody":"WidgetWarning-module_popoverBody__2d-rw","tooltipContent":"WidgetWarning-module_tooltipContent__RW_dR","container":"WidgetWarning-module_container__jCrC1","warningIcon":"WidgetWarning-module_warningIcon__GxgmZ"};

var WidgetWarning = function WidgetWarning(_ref) {
  var warning = _ref.warning;
  return React.createElement("div", {
    className: styles$3.container
  }, React.createElement(OverlayTrigger, {
    trigger: ['hover', 'focus'],
    placement: "auto",
    overlay: React.createElement(Popover, {
      id: "warning-popover",
      className: styles$3.tooltipContainer
    }, React.createElement(Popover.Content, null, React.createElement("p", {
      className: styles$3.tooltipContent
    }, warning)))
  }, React.createElement("div", {
    className: styles$3.warningIcon
  }, React.createElement("i", {
    className: classnames('fas fa-exclamation-triangle')
  }))));
};

var WidgetContainer = function WidgetContainer(_ref) {
  var rss = _ref.rss,
      asyncRender = _ref.asyncRender,
      children = _ref.children,
      title = _ref.title,
      _ref$titleScale = _ref.titleScale,
      titleScale = _ref$titleScale === void 0 ? 1 : _ref$titleScale,
      className = _ref.className,
      contentContainerClassName = _ref.contentContainerClassName,
      contentContainerStyle = _ref.contentContainerStyle,
      _ref$margin = _ref.margin,
      margin = _ref$margin === void 0 ? [0, 0, 0, 0] : _ref$margin,
      _ref$padding = _ref.padding,
      padding = _ref$padding === void 0 ? [0, 0, 0, 0] : _ref$padding,
      _ref$border = _ref.border,
      border = _ref$border === void 0 ? [{}, {}, {}, {}] : _ref$border,
      backgroundColor = _ref.backgroundColor,
      style = _ref.style,
      containerRef = _ref.containerRef,
      titleColor = _ref.titleColor,
      titleBackgroundColor = _ref.titleBackgroundColor,
      warning = _ref.warning,
      titleAlign = _ref.titleAlign,
      rest = _objectWithoutPropertiesLoose(_ref, ["rss", "asyncRender", "children", "title", "titleScale", "className", "contentContainerClassName", "contentContainerStyle", "scale", "margin", "padding", "border", "backgroundColor", "style", "containerRef", "titleColor", "titleBackgroundColor", "warning", "titleAlign"]);

  var _useState = useState(false),
      rendered = _useState[0],
      setRendered = _useState[1];

  var hasRendered = function hasRendered() {
    setRendered(true);
  };

  useEffect(function () {
    if (!asyncRender) hasRendered();else setTimeout(function () {
      if (!rendered) hasRendered();
    }, 5000);
  }, []);
  var childrenWithProps = React.Children.map(children, function (child) {
    if (isValidElement(child)) return cloneElement(child, {
      hasRendered: hasRendered
    });
    return child;
  });
  var widthMargin = Number(margin[1]) + Number(margin[3]);
  var heightMargin = Number(margin[0]) + Number(margin[2]);
  var marginStr = buildStyleString(margin, 'px');
  var paddingStr = buildStyleString(padding, 'px');
  var borderStyle = buildBorderStyle(border);
  return React.createElement("div", {
    className: styles$1.widgetContainerContainer,
    style: {
      backgroundColor: hex(backgroundColor)
    }
  }, warning && React.createElement(WidgetWarning, {
    warning: warning
  }), React.createElement("div", Object.assign({
    className: styles$1.widgetContainer + ' ' + (className ? className : ''),
    style: _extends({}, style, {
      width: "calc(100% - " + widthMargin + "px)",
      height: "calc(100% - " + heightMargin + "px)",
      margin: marginStr,
      padding: paddingStr,
      borderStyle: 'solid',
      borderWidth: borderStyle.width,
      borderColor: borderStyle.color
    })
  }, rest), rss && rendered && React.createElement("span", {
    id: "rendered"
  }), title && React.createElement(WidgetHeader, {
    title: title,
    scale: titleScale,
    titleColor: titleColor,
    titleBackgroundColor: titleBackgroundColor,
    titleAlign: titleAlign
  }), React.createElement("div", {
    className: styles$1.widgetContentContainer + ' ' + (contentContainerClassName ? contentContainerClassName : ''),
    style: _extends({}, contentContainerStyle),
    ref: containerRef
  }, typeof children === 'function' ? children({
    hasRendered: hasRendered
  }) : childrenWithProps)));
};

var HEADER_FONT_SIZE = 24;

var Header = function Header(_ref) {
  var rss = _ref.rss,
      content = _ref.content,
      _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale,
      margin = _ref.margin,
      padding = _ref.padding,
      border = _ref.border,
      backgroundColor = _ref.backgroundColor,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? 'ff8b49' : _ref$color,
      _ref$textAlign = _ref.textAlign,
      textAlign = _ref$textAlign === void 0 ? DEFAULT_ALIGN : _ref$textAlign,
      _ref$renderHtml = _ref.renderHtml,
      renderHtml = _ref$renderHtml === void 0 ? false : _ref$renderHtml;
  return React.createElement(WidgetContainer, {
    rss: rss,
    scale: scale,
    margin: margin,
    padding: padding,
    border: border,
    backgroundColor: backgroundColor
  }, React.createElement("div", {
    className: styles.container,
    style: {
      alignItems: getFlexOrientation(textAlign)
    }
  }, !renderHtml ? React.createElement("h2", {
    className: styles.header,
    style: {
      fontSize: HEADER_FONT_SIZE * scale,
      color: hex(color),
      textAlign: getTextAlign(textAlign)
    }
  }, content) : React.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: sanitizeHtml(content, SANITIZE_OPTIONS)
    }
  })));
};

var allowGenericAttributes = function allowGenericAttributes(attributes) {
  if (attributes === void 0) {
    attributes = [];
  }

  var tags = ['address', 'article', 'aside', 'footer', 'header', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hgroup', 'main', 'nav', 'section', 'blockquote', 'dd', 'div', 'dl', 'dt', 'figcaption', 'figure', 'hr', 'li', 'main', 'ol', 'p', 'pre', 'ul', 'a', 'abbr', 'b', 'bdi', 'bdo', 'br', 'cite', 'code', 'data', 'dfn', 'em', 'i', 'kbd', 'mark', 'q', 'rb', 'rp', 'rt', 'rtc', 'ruby', 's', 'samp', 'small', 'span', 'strong', 'sub', 'sup', 'time', 'u', 'var', 'wbr', 'caption', 'col', 'colgroup', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr'];
  var ret = {};
  tags.forEach(function (t) {
    ret[t] = attributes;
  });
  return ret;
};

var genericAttributes = /*#__PURE__*/allowGenericAttributes(['style']);
genericAttributes.a = ['href', 'name', 'target', 'style'];
genericAttributes.img = ['src', 'style'];
var SANITIZE_OPTIONS = {
  allowedTags: /*#__PURE__*/sanitizeHtml.defaults.allowedTags.concat(['img']),
  allowedAttributes: genericAttributes
};

var styles$4 = {"table":"Table-module_table__1mwWR","heading":"Table-module_heading__2ndlt","row":"Table-module_row__1maEJ","cell":"Table-module_cell__1lUbX","rowSeparator":"Table-module_rowSeparator__1iW50","label":"Table-module_label__3Ucjs"};

var LABEL_FONT_SIZE = 12;
var DATA_FONT_SIZE = 12;
var LABEL_WIDTH = 80;
var DAY_MILLI = 90000000;
var ROW_HEIGHT = 36;

var TableTecno = function TableTecno(_ref) {
  var _data$data;

  var rss = _ref.rss,
      data = _ref.data,
      title = _ref.title,
      titleScale = _ref.titleScale,
      _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale,
      margin = _ref.margin,
      padding = _ref.padding,
      border = _ref.border,
      _ref$footerRows = _ref.footerRows,
      footerRows = _ref$footerRows === void 0 ? {
    sums: true,
    avgs: true,
    mins: true,
    maxs: true
  } : _ref$footerRows,
      decimalLimit = _ref.decimalLimit,
      _ref$globalParameters = _ref.globalParameters,
      globalParameters = _ref$globalParameters === void 0 ? {} : _ref$globalParameters,
      _ref$dateTimeFormat = _ref.dateTimeFormat,
      dateTimeFormat = _ref$dateTimeFormat === void 0 ? 'DATETIME' : _ref$dateTimeFormat,
      dataLimit = _ref.dataLimit,
      titleBackgroundColor = _ref.titleBackgroundColor,
      titleColor = _ref.titleColor,
      backgroundColor = _ref.backgroundColor,
      hideDate = _ref.hideDate,
      hideHeader = _ref.hideHeader,
      titleAlign = _ref.titleAlign;
  var timezone = globalParameters.timezone;
  if (_.isEmpty(data)) return null;
  if (dataLimit) data.data = dataLimiter(data.data, dataLimit);
  var isDayInterval = false;

  try {
    var firstDay = data.data[0].timestamp;
    var lastDay = data.data[data.data.length - 1].timestamp;
    isDayInterval = Math.abs(lastDay - firstDay) < DAY_MILLI;
  } catch (error) {}

  var dateFormat = getDateTimeFormat(dateTimeFormat);
  var footerData = getFooterData(data, footerRows);
  var nValues = (data == null ? void 0 : (_data$data = data.data) == null ? void 0 : _data$data.length) > 0 ? data.data[0].values.length : 0;
  var rowStyle = {
    height: ROW_HEIGHT,
    gridTemplateColumns: (!hideDate ? '100px ' : '') + Array(nValues).fill(0).reduce(function (str) {
      return str + '1fr ';
    }, '')
  };
  if (rss) return React.createElement(WidgetContainer, {
    rss: rss,
    title: title,
    titleScale: titleScale,
    scale: scale,
    margin: margin,
    padding: padding,
    border: border,
    titleBackgroundColor: titleBackgroundColor,
    titleColor: titleColor,
    backgroundColor: backgroundColor,
    titleAlign: titleAlign
  }, React.createElement(SSTable, {
    data: data,
    footerData: footerData,
    rowStyle: rowStyle,
    timezone: timezone,
    dateFormat: dateFormat,
    decimalLimit: decimalLimit,
    scale: scale,
    hideDate: hideDate,
    hideHeader: hideHeader
  }));
  return React.createElement(WidgetContainer, {
    rss: rss,
    title: title,
    scale: scale,
    margin: margin,
    padding: padding,
    border: border,
    titleBackgroundColor: titleBackgroundColor,
    titleColor: titleColor,
    backgroundColor: backgroundColor,
    titleAlign: titleAlign
  }, React.createElement(SizeMe, {
    monitorHeight: true,
    monitorWidth: false
  }, function (_ref2) {
    var size = _ref2.size;
    return React.createElement("div", {
      className: styles$4.table
    }, !hideHeader && React.createElement("div", {
      className: styles$4.heading,
      style: rowStyle
    }, !hideDate && React.createElement("div", {
      className: classnames(styles$4.cell, styles$4.label),
      style: {
        width: LABEL_WIDTH * scale
      }
    }), data.headers.map(function (header, i) {
      return React.createElement("div", {
        key: i,
        className: styles$4.cell,
        style: {
          fontSize: LABEL_FONT_SIZE * scale
        }
      }, header.label || header.id);
    })), React.createElement(FixedSizeList, {
      height: size.height || 0,
      width: '100%',
      itemCount: data.data.length + footerData.length,
      itemSize: ROW_HEIGHT
    }, function (_ref3) {
      var index = _ref3.index,
          style = _ref3.style;
      return index < data.data.length ? React.createElement(TableRow, {
        key: index,
        label: getMomentTF(data.data[index].timestamp, timezone).format(dateFormat),
        data: data.data[index].values,
        rowStyle: _extends({}, rowStyle, style),
        scale: scale,
        decimalLimit: decimalLimit,
        hideLabel: hideDate
      }) : React.createElement(TableRow, {
        key: index,
        label: footerData[index - data.data.length].label,
        data: footerData[index - data.data.length].data,
        rowStyle: _extends({}, rowStyle, style),
        scale: scale,
        decimalLimit: decimalLimit,
        separator: index === data.data.length,
        hideLabel: hideDate
      });
    }));
  }));
};

var TableRow = function TableRow(_ref4) {
  var data = _ref4.data,
      label = _ref4.label,
      separator = _ref4.separator,
      scale = _ref4.scale,
      decimalLimit = _ref4.decimalLimit,
      rowStyle = _ref4.rowStyle,
      hideLabel = _ref4.hideLabel;
  return React.createElement("div", {
    className: classnames(styles$4.row, separator ? styles$4.rowSeparator : undefined),
    style: rowStyle
  }, !hideLabel && React.createElement("div", {
    className: classnames(styles$4.cell, styles$4.label),
    style: {
      fontSize: LABEL_FONT_SIZE * scale,
      width: LABEL_WIDTH * scale
    }
  }, label), data.map(function (v, index) {
    return React.createElement("div", {
      className: styles$4.cell,
      key: index,
      style: {
        fontSize: DATA_FONT_SIZE * scale
      }
    }, shortValue(v, decimalLimit));
  }));
};

var SSTable = function SSTable(_ref5) {
  var rowStyle = _ref5.rowStyle,
      data = _ref5.data,
      footerData = _ref5.footerData,
      timezone = _ref5.timezone,
      dateFormat = _ref5.dateFormat,
      decimalLimit = _ref5.decimalLimit,
      scale = _ref5.scale,
      hideDate = _ref5.hideDate,
      hideHeader = _ref5.hideHeader;
  return React.createElement("div", {
    className: classnames(styles$4.table, 'RE-scrollable-container')
  }, !hideHeader && React.createElement("div", {
    className: styles$4.heading,
    style: rowStyle
  }, !hideDate && React.createElement("div", {
    className: classnames(styles$4.cell, styles$4.label),
    style: {
      width: LABEL_WIDTH * scale
    }
  }), data.headers.map(function (header, i) {
    return React.createElement("div", {
      key: i,
      className: styles$4.cell,
      style: {
        fontSize: LABEL_FONT_SIZE * scale
      }
    }, header.label || header.id);
  })), React.createElement("div", null, data.data.map(function (_ref6, index) {
    var timestamp = _ref6.timestamp,
        values = _ref6.values;
    return React.createElement(TableRow, {
      key: index,
      label: getMomentTF(timestamp, timezone).format(dateFormat),
      data: values,
      rowStyle: rowStyle,
      scale: scale,
      decimalLimit: decimalLimit,
      hideLabel: hideDate
    });
  }), footerData.map(function (_ref7, index) {
    var label = _ref7.label,
        data = _ref7.data;
    return React.createElement(TableRow, {
      key: index,
      label: label,
      data: data,
      rowStyle: rowStyle,
      scale: scale,
      decimalLimit: decimalLimit,
      separator: index === 0,
      hideLabel: hideDate
    });
  })));
};

var getFooterData = function getFooterData(_ref8, footerRows) {
  var sums = _ref8.sums,
      avgs = _ref8.avgs,
      mins = _ref8.mins,
      maxs = _ref8.maxs;
  var footerData = [];
  if (sums && footerRows.sums) footerData.push({
    label: 'Sum',
    data: sums
  });
  if (avgs && footerRows.avgs) footerData.push({
    label: 'Avg',
    data: avgs
  });
  if (mins && footerRows.mins) footerData.push({
    label: 'Min',
    data: mins
  });
  if (maxs && footerRows.maxs) footerData.push({
    label: 'Max',
    data: maxs
  });
  return footerData;
};

export { DEFAULT_COLORS as D, Header as H, LABEL_FONT_SIZE as L, TableTecno as T, WidgetContainer as W, _extends as _, getMomentTF as a, safetyLimit as b, getDateTimeFormat as c, dataLimiter as d, getTextAlign as e, DEFAULT_LEFT_ALIGN as f, getTimeframe as g, hex as h, interpolator as i, nowDateTF as j, _objectWithoutPropertiesLoose as k, limitDecimals as l, advancedBorder as m, numberWithCommas as n, buildStyleString as o, WIDGET_TITLE_SIZE as p, border as q, HEADER_FONT_SIZE as r, shortValue as s, DEFAULT_ALIGN as t, DATA_FONT_SIZE as u, getFooterData as v };
//# sourceMappingURL=Table-28c5e8c7.js.map
