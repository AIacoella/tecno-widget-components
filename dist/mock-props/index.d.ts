declare const _default: {
    TimeSeriesTecno: import("../TecnoWidgets/Timeseries/LineChart/LineChart").LineChartProps;
    ParagraphTecno: import("../TecnoWidgets/Paragraph/Paragraph").ParagraphProps;
    HeaderTecno: import("../TecnoWidgets/Header/Header").HeaderProps;
    TableTecno: import("../TecnoWidgets/Table/Table").TableTecnoProps;
    Display: import("../TecnoWidgets/Display/Display").DisplayProps;
    BarChart: import("../TecnoWidgets/Timeseries/BarChart/BarChart").BarChartProps;
    Clock: import("../TecnoWidgets/Clock/Clock").ClockProps;
    Image: import("../TecnoWidgets/Image/Image").ImageProps;
    PieChart: import("../TecnoWidgets/PieChart/PieChart").PieChartProps;
    MultiChart: import("../TecnoWidgets/Timeseries/MultiChart/MultiChart").MultiChartProps;
};
export default _default;
