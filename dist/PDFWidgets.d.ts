import HeaderTecnoPDF from './TecnoWidgets/Header/HeaderPDF';
import TableTecnoPDF from './TecnoWidgets/Table/TablePDF';
import ParagraphTecnoPDF from './TecnoWidgets/Paragraph/ParagraphPDF';
export { HeaderTecnoPDF, TableTecnoPDF, ParagraphTecnoPDF };
