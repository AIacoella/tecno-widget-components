import React from 'react';
import { GlobalParametersInterface, OptionType } from './utils';
interface WidgetContainerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    rss?: boolean;
    asyncRender?: boolean;
    title?: string;
    titleScale?: number;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    contentContainerClassName?: string;
    contentContainerStyle?: React.CSSProperties;
    scale: number;
    margin?: FourIntegerType;
    padding?: FourIntegerType;
    border?: FourBorderType;
    backgroundColor?: string;
    containerRef?: React.MutableRefObject<any>;
    titleColor?: string;
    titleBackgroundColor?: string;
    warning?: string;
}
export interface WidgetProps {
    rss?: boolean;
    scale: number;
    margin?: FourIntegerType;
    padding?: FourIntegerType;
    border?: FourBorderType;
    backgroundColor?: string;
    globalParameters?: GlobalParametersInterface;
}
declare type V = string | number;
export declare type FourIntegerType = [V, V, V, V];
declare type BT = {
    borderWidth?: string | number;
    borderColor?: string;
};
export declare type FourBorderType = [BT, BT, BT, BT];
export declare const WidgetContainer: React.FunctionComponent<WidgetContainerProps>;
export {};
