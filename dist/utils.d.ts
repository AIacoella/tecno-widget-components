import moment, { Moment } from 'moment-timezone';
import { FourBorderType, FourIntegerType } from './Widget';
export declare const dataLimiter: (data: any[], limit?: number) => any[];
export declare const safetyLimit: (data: any[], numSources?: number) => any[];
export declare const buildStyleString: (data: FourIntegerType, suffix?: string) => string | number;
export declare const buildBorderStyle: (value: FourBorderType) => {
    width: string;
    color: string;
};
export declare const border: (color: string, sides?: number[], style?: 'solid' | 'dashed') => any;
export declare const advancedBorder: (value: FourBorderType) => any;
export declare const getMomentTF: (inp: any, timezone?: string | undefined) => moment.Moment;
export declare const nowDateTF: (timezone?: string | undefined) => Date;
declare type ValueUomType = {
    value: number;
    uom: string;
};
export interface TimeframeInterface {
    name: string;
    from?: string;
    to?: string;
    previousOffset?: ValueUomType;
    interval?: ValueUomType;
    offsetFrom?: ValueUomType;
    startTime?: string;
    endTime?: string;
}
export interface GlobalParametersInterface {
    timezone?: string;
    metrics?: string[];
    company?: string;
    timeframe?: TimeframeInterface;
}
export declare const limitDecimals: (value: number | string, decimalLimiter?: number | false | undefined) => number;
export declare function numberWithCommas(x: number): string;
export declare const shortValue: (value?: string | number | undefined, decimalLimit?: number | false | undefined) => number;
export declare const hex: (value?: string | undefined) => string | undefined;
export declare const DEFAULT_COLORS: string[];
export declare const getDateTimeFormat: (dateTimeFormat: 'DATE' | 'TIME' | 'DATETIME') => "DD/MM" | "HH:mm" | "DD/MM HH:mm";
export declare type OptionType<V = string> = {
    value: V;
    label: string;
};
export declare const DEFAULT_ALIGN: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
export declare const DEFAULT_LEFT_ALIGN: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
export declare const getTextAlign: (alignText: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>) => 'left' | 'right' | 'center';
export declare const getFlexOrientation: (alignText: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>) => "center" | "flex-start" | "flex-end";
export declare const interpolator: (data: {
    [key: string]: number;
    timestamp: number;
}[], ids: string[]) => {
    [key: string]: number;
    timestamp: number;
}[];
export declare const getTimeframe: (timeframe: any, timezone?: string | undefined) => {
    from: number | undefined;
    to: number;
};
export declare const vivifyTimeframe: (timeframe: any, timezone: string) => any;
export declare const processDynamicOffset: (date: Moment, dynamicOffset: number, uom: string) => moment.Moment;
export {};
