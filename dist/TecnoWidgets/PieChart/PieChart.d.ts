import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
interface PieChartData {
    name: string;
    value: number;
    color?: string;
    label?: string;
}
export interface PieChartProps extends WidgetProps {
    data: PieChartData[];
    title?: string;
    titleScale?: number;
    showLegend?: boolean;
    titleColor?: string;
    titleBackgroundColor?: string;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
declare const PieChart: React.FunctionComponent<PieChartProps>;
export default PieChart;
