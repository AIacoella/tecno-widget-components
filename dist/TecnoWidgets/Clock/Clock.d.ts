import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
export declare const FONT_SIZE = 14;
export interface ClockProps extends WidgetProps {
    title?: string;
    titleScale?: number;
    titleColor?: string;
    titleBackgroundColor?: string;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
declare const Clock: React.FunctionComponent<ClockProps>;
export default Clock;
