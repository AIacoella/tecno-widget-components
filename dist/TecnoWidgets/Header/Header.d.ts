import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
export interface HeaderProps extends WidgetProps {
    content: string;
    color?: string;
    textAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    renderHtml?: boolean;
}
export declare const HEADER_FONT_SIZE = 24;
declare const Header: React.FunctionComponent<HeaderProps>;
export default Header;
