/// <reference types="react" />
import { HeaderProps } from './Header';
interface HeaderPDFProps extends HeaderProps {
}
export default function HeaderPDF({ content, scale, border, margin, padding, color, backgroundColor, textAlign, }: HeaderPDFProps): JSX.Element;
export {};
