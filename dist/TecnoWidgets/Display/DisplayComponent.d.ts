import React from 'react';
export declare type FormatIntervalType = {
    bound: number;
    color: string;
};
export interface DisplayComponentProps {
    data?: number | string;
    prefix?: string;
    suffix?: string;
    label?: string;
    decimalLimit?: false | number;
    color?: string;
    valueFormat?: {
        value: string;
    };
    emptyValue?: string;
    formatIntervals?: FormatIntervalType[];
    scale?: number;
    className?: string;
    style?: React.CSSProperties;
}
export declare const DISPLAY_FONT_SIZE = 24;
export declare const DISPLAY_LABEL_SIZE = 14;
export declare const DisplayComponent: React.FunctionComponent<DisplayComponentProps>;
export declare const VALUE_FORMATS: {
    NUMBER: string;
    HOURS_FULL: string;
    HOURS: string;
    MINUTES_FULL: string;
    MINUTES: string;
    SECONDS_FULL: string;
    SECONDS: string;
};
