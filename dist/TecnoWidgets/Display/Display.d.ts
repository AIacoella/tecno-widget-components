import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
import { FormatIntervalType } from './DisplayComponent';
export interface DisplayProps extends WidgetProps {
    data?: number | string;
    prefix?: string;
    suffix?: string;
    label?: string;
    title?: string;
    titleScale?: number;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    decimalLimit?: false | number;
    color?: string;
    valueFormat?: {
        value: string;
    };
    emptyValue?: string;
    titleColor?: string;
    titleBackgroundColor?: string;
    formatIntervals?: FormatIntervalType[];
}
declare const Display: React.FunctionComponent<DisplayProps>;
export default Display;
