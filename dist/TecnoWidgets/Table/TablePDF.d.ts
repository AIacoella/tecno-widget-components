/// <reference types="react" />
import { TableTecnoProps } from './Table';
export default function TablePDF({ data, scale, title, titleScale, border: borderData, margin, padding, decimalLimit, footerRows, globalParameters, dateTimeFormat, titleColor, titleBackgroundColor, backgroundColor, hideDate, hideHeader, titleAlign, }: TableTecnoProps): JSX.Element;
