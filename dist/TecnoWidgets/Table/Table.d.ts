import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
export declare const LABEL_FONT_SIZE = 12;
export declare const DATA_FONT_SIZE = 12;
export declare const LABEL_WIDTH = 80;
interface HeaderInterface {
    id: string;
    label?: string;
}
declare type FooterRowType = {
    sums: boolean;
    avgs: boolean;
    mins: boolean;
    maxs: boolean;
};
interface TableDataInterface {
    headers: HeaderInterface[];
    data: {
        timestamp: number;
        values: number[];
    }[];
    sums?: number[];
    avgs?: number[];
    mins?: number[];
    maxs?: number[];
}
export interface TableTecnoProps extends WidgetProps {
    data: TableDataInterface;
    footerRows?: FooterRowType;
    title?: string;
    titleScale?: number;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    decimalLimit?: false | number;
    dateTimeFormat?: 'DATE' | 'TIME' | 'DATETIME';
    dataLimit?: number;
    titleColor?: string;
    titleBackgroundColor?: string;
    hideDate?: boolean;
    hideHeader?: boolean;
}
declare const TableTecno: React.FunctionComponent<TableTecnoProps>;
export default TableTecno;
export declare const getFooterData: ({ sums, avgs, mins, maxs, }: {
    sums?: number[] | undefined;
    avgs?: number[] | undefined;
    mins?: number[] | undefined;
    maxs?: number[] | undefined;
}, footerRows: FooterRowType) => {
    label: string;
    data: number[];
}[];
