import React from 'react';
import { TimeSeriesProps, SingelChartData, DomainType } from '../types';
export interface LineChartProps extends TimeSeriesProps<SingelChartData> {
    hideLineDot?: boolean;
    yDomain?: DomainType;
}
declare const LineChart: React.FunctionComponent<LineChartProps>;
export default LineChart;
