import React from 'react';
import { TimeframeInterface } from '../../utils';
import { AxisType, ReferenceLineType } from './types';
export declare const LABEL_SIZE = 13;
export declare const AXIS_LABEL_SIZE = 14;
interface AxisProps {
    hideXAxis?: boolean;
    hideYAxis?: boolean;
    autoDateFormat: string;
    scale: number;
    timezone?: string;
    xAxisLabel?: string;
    yAxisLabel?: string;
    decimalLimit?: false | number;
}
interface ChartProps extends AxisProps {
    dateFormat: 'HH:mm' | 'DD/MM' | 'DD/MM HH:mm';
    hasData: boolean;
    setBrushValue?: (pos: {
        startIndex: number;
        endIndex: number;
    }) => {};
    brushValue?: {
        startIndex: number;
        endIndex: number;
    };
    setIsDraggable?: (isDraggable: boolean) => {};
    showLegend?: boolean;
    ChartComponent: any;
    data: any;
    yDomain?: {
        min: string | number;
        max: string | number;
    };
    yAxis?: AxisType[];
    rss?: boolean;
    showBrush?: boolean;
    referenceLines?: ReferenceLineType[];
    timeframe?: TimeframeInterface;
    adjustDomain?: boolean;
}
declare const Chart: React.FunctionComponent<ChartProps>;
export default Chart;
export declare const timestampFormatter: (dateFormat: string, timezone?: string | undefined) => (timestamp: number | string) => string;
export declare const CustomTickYAxis: ({ x, y, payload, scale, decimalLimit, orientation, }: any) => JSX.Element;
export declare const CustomTickXAxis: ({ x, y, payload, scale, timezone, dateFormat, }: any) => JSX.Element;
export declare const SAFETY_LIMIT_WARNING = "Safety sub-sampling enabled";
