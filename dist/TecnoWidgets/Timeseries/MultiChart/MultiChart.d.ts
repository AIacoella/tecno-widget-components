import React from 'react';
import { TimeSeriesProps, MultiChartData, AxisType } from '../types';
export interface MultiChartProps extends TimeSeriesProps<MultiChartData> {
    hideLineDot?: boolean;
    yAxis?: AxisType[];
}
declare const MultiChart: React.FunctionComponent<MultiChartProps>;
export default MultiChart;
