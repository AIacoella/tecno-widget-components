import React from 'react';
import { DomainType, SingelChartData, TimeSeriesProps } from '../types';
export interface BarChartProps extends TimeSeriesProps<SingelChartData> {
    yDomain?: DomainType;
}
declare const BarChart: React.FunctionComponent<BarChartProps>;
export default BarChart;
