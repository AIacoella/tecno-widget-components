import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
export declare const FONT_SIZE = 14;
export interface ParagraphProps extends WidgetProps {
    content: string;
    className?: string;
    textAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
declare const Paragraph: React.FunctionComponent<ParagraphProps>;
export default Paragraph;
