/// <reference types="react" />
import { ParagraphProps } from './Paragraph';
interface ParagraphPDFProps extends ParagraphProps {
}
export default function ParagraphPDF({ content, scale, border, margin, padding, textAlign, backgroundColor, }: ParagraphPDFProps): JSX.Element;
export {};
