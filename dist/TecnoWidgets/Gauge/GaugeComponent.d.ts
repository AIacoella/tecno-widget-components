import React from 'react';
export declare type FormatIntervalType = {
    bound: number;
    color: string;
};
export interface GaugeComponentProps {
    data?: number | string;
    prefix?: string;
    suffix?: string;
    label?: string;
    decimalLimit?: false | number;
    color?: string;
    valueFormat?: {
        value: string;
    };
    emptyValue?: string;
    sections?: {
        bound: number;
        color: string;
    }[];
    scale?: number;
}
export declare const GAUGE_FONT_SIZE = 24;
export declare const GAUGE_LABEL_SIZE = 14;
export declare const GaugeComponent: React.FunctionComponent<GaugeComponentProps>;
