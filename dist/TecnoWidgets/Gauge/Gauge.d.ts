import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
export interface GaugeProps extends WidgetProps {
    data?: number | string;
    prefix?: string;
    suffix?: string;
    label?: string;
    title?: string;
    titleScale?: number;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    decimalLimit?: false | number;
    color?: string;
    valueFormat?: {
        value: string;
    };
    emptyValue?: string;
    titleColor?: string;
    titleBackgroundColor?: string;
    sections?: {
        bound: number;
        color: string;
    }[];
}
declare const Gauge: React.FunctionComponent<GaugeProps>;
export default Gauge;
