import React from 'react';
import { WidgetProps } from '../../Widget';
import { OptionType } from '../../utils';
import { DisplayComponentProps } from '../Display/DisplayComponent';
import { GaugeComponentProps } from '../Gauge/GaugeComponent';
export declare const FONT_SIZE = 14;
interface PanelSource {
    type: 'DISPLAY' | 'GAUGE';
    x: number;
    y: number;
    parameters: DisplayComponentProps | GaugeComponentProps;
}
declare type DataType = number | string;
export interface PanelProps extends WidgetProps {
    data?: DataType[];
    sources: PanelSource[];
    url?: string;
    objectFit?: {
        value: 'cover' | 'contain' | 'fill';
        label: string;
    };
    title?: string;
    titleScale?: number;
    titleColor?: string;
    titleBackgroundColor?: string;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
declare const Panel: React.FunctionComponent<PanelProps>;
export default Panel;
