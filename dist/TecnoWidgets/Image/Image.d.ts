import React from 'react';
import { OptionType } from 'utils';
import { WidgetProps } from '../../Widget';
export interface ImageProps extends WidgetProps {
    title?: string;
    titleScale?: number;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
    url?: string;
    objectFit?: {
        value: 'cover' | 'contain' | 'fill';
        label: string;
    };
    titleColor?: string;
    titleBackgroundColor?: string;
}
export declare const LABEL_SIZE = 13;
export declare const AXIS_LABEL_SIZE = 14;
declare const Image: React.FunctionComponent<ImageProps>;
export default Image;
