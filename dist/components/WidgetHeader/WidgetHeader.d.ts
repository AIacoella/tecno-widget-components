import React from 'react';
import { OptionType } from '../../utils';
export declare const WIDGET_TITLE_SIZE = 24;
interface WidgetHeaderProps {
    title?: string;
    scale: number;
    titleColor?: string;
    titleBackgroundColor?: string;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
declare const WidgetHeader: React.FunctionComponent<WidgetHeaderProps>;
export default WidgetHeader;
