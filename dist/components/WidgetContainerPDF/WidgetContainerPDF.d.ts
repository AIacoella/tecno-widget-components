import React from 'react';
import ReactPDF from '@react-pdf/renderer';
import { OptionType } from '../../utils';
declare type V = string | number;
declare type FourIntegerType = [V, V, V, V];
declare type BT = {
    borderWidth?: string | number;
    borderColor?: string;
};
declare type FourBorderType = [BT, BT, BT, BT];
interface WidgetContainerPDFProps extends ReactPDF.ViewProps {
    title?: string;
    titleScale?: number;
    scale: number;
    children?: React.ReactElement;
    style?: any;
    margin?: FourIntegerType;
    padding?: FourIntegerType;
    border?: FourBorderType;
    backgroundColor?: string;
    titleColor?: string;
    titleBackgroundColor?: string;
    titleAlign?: OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
}
export default function WidgetContainerPDF({ title, titleScale, scale, children, style, margin, padding, border: borderData, backgroundColor, titleBackgroundColor, titleColor, titleAlign, ...rest }: WidgetContainerPDFProps): JSX.Element;
export {};
