import React from 'react';
import { OptionType } from '../../utils';
declare type AlignTextType = OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
interface PDFMarkdownProps {
    source: string;
    scale?: number;
    textAlign: AlignTextType;
}
declare const PDFMarkdown: React.FunctionComponent<PDFMarkdownProps>;
export default PDFMarkdown;
