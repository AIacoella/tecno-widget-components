import React from 'react';
export declare const WIDGET_TITLE_SIZE = 24;
interface WidgetWarningProps {
    warning: string;
}
declare const WidgetWarning: React.FunctionComponent<WidgetWarningProps>;
export default WidgetWarning;
