import React from 'react';
import { IconName } from '@fortawesome/fontawesome-svg-core';
interface AvatrButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    icon?: IconName | IconRendererType;
    solid?: boolean;
}
declare const AvatrButton: React.FunctionComponent<AvatrButtonProps>;
export default AvatrButton;
declare type IconRendererType = () => React.ReactElement;
