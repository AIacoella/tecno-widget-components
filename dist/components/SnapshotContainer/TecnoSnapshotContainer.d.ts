import React from 'react';
import { WidgetProps } from '../../Widget';
interface TecnoSnapshotContainerProps extends WidgetProps {
    title?: string;
}
declare const TecnoSnapshotContainer: React.FunctionComponent<TecnoSnapshotContainerProps>;
export default TecnoSnapshotContainer;
