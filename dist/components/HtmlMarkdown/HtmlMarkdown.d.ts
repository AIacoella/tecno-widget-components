import React from 'react';
import { OptionType } from '../../utils';
declare type AlignTextType = OptionType<'LEFT' | 'CENTER' | 'RIGHT'>;
interface HtmlMarkdownProps {
    source: string;
    className?: string;
    scale?: number;
    textAlign: AlignTextType;
}
declare const HtmlMarkdown: React.FunctionComponent<HtmlMarkdownProps>;
export default HtmlMarkdown;
